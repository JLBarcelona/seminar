<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSeminarGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_seminar_groups', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('group_name', 45)->nullable();
            $table->date('registration_from')->nullable();
            $table->date('registration_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_seminar_groups');
    }
}
