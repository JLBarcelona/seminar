<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSeminarStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_seminar_status', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('seminar_registration_id')->nullable();
            $table->dateTime('reception_in_at')->nullable();
            $table->string('reception_in_err', 128)->nullable();
            $table->dateTime('exam_answered_at')->nullable();
            $table->dateTime('reception_out_at')->nullable();
            $table->string('reception_out_err', 128)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_seminar_status');
    }
}
