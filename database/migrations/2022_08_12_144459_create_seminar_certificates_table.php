<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeminarCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_certificates', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('m_seminar_group_id')->nullable();
            $table->integer('m_seminar_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('total_exam_count')->nullable();
            $table->integer('correct_answer_count')->nullable();
            $table->dateTime('exam_at')->nullable();
            $table->integer('passed_exam')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_certificates');
    }
}
