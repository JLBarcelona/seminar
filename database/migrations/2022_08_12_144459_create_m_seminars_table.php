<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSeminarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_seminars', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('m_seminar_group_id')->nullable();
            $table->integer('category_no')->nullable();
            $table->string('category_name', 128)->nullable();
            $table->string('seminar_title', 128)->nullable();
            $table->decimal('seminar_hours', 3, 1)->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('end_at')->nullable();
            $table->integer('capacity')->nullable();
            $table->date('seminar_date')->nullable();
            $table->dateTime('reception_open_at')->nullable();
            $table->dateTime('reception_close_at')->nullable();
            $table->string('teacher_name', 45)->nullable();
            $table->string('teacher_affiliation1', 45)->nullable();
            $table->string('teacher_affiliation2', 45)->nullable();
            $table->string('teacher_affiliation3', 45)->nullable();
            $table->string('place', 45)->nullable();
            $table->string('movie_url', 256)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_seminars');
    }
}
