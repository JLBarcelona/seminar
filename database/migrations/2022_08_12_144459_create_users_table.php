<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('l_name', 45)->nullable();
            $table->string('f_name', 45)->nullable();
            $table->string('l_name_kana', 45)->nullable();
            $table->string('f_name_kana', 45)->nullable();
            $table->string('certificate_id', 45)->nullable();
            $table->string('registration_id', 45)->nullable();
            $table->string('mail_address', 128)->nullable();
            $table->string('hospital_name', 128)->nullable();
            $table->string('devision_name', 128)->nullable();
            $table->string('tel', 45)->nullable();
            $table->date('birthday')->nullable();
            $table->string('password', 256)->nullable();
            $table->dateTime('email_check_at')->nullable();
            $table->string('email_verification_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
