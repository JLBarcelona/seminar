<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeminarRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_registrations', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('m_seminar_group_id')->nullable();
            $table->integer('m_seminar_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('qr_code', 256)->nullable();
            $table->dateTime('registration_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_registrations');
    }
}
