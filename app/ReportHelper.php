<?php 
function certificatePdf($full_name,$m_seminar_group_id){
    $output = '
        <!DOCTYPE html>
        <html lang="jp">
           <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <style>
                  .border-img{
                    background-image: url('.asset('img/border.png').');
                    background-position: center;
                    background-repeat: no-repeat;
                    background-size: cover;
                    height: 100%;
                    width: 100%;
                  }
                 .justify_content{
                    padding: 140px;
                 }

                 .text-center{
                  text-align: center;
                 }

                 .mb100{
                  margin-bottom: 100px;
                 }

                 .mb50{
                  margin-bottom: 50px;
                 }

                 .mb15{
                  margin-bottom: 15px;
                 }

                 .text-left{
                  text-align: left;
                 }

                 .text-right{
                  text-align: right;
                 }

              </style>
           </head>
           <body>
              <div class="border-img">
                <div class="justify_content" style="">
                  <p class="text-center mb100" style="font-size:30px;">指導医養成講座　受講証明書</p>

                  <p class="text-center" >貴殿が、2022 年⽇本胆道学会 指導医養成講座</p>
                  <p class="text-center mb100" >を受講し合格したことを証明します。</p>';

                  // Certicate Content

                  if($m_seminar_group_id == 1){
                    $output .=' <p>分野／領域　総　論</p>
                                <p class="mb15">タイトル　胆道感染における抗菌薬の適正使用</p>
                              ';
                  }else if($m_seminar_group_id == 2){
                    $output .='<p>分野／領域　内視鏡的・経皮経肝的診断・治療</p>
                               <p class="mb15">タイトル　総胆管結石の診断と治療</p>
                              ';
                  }else if($m_seminar_group_id == 3){
                    $output .='<p>分野／領域　薬 物 治 療</p>
                               <p class="mb15">タイトル　化 学 療 法</p>
                              ';
                  }else if($m_seminar_group_id == 4){
                    $output .='<p>分野／領域　良性・悪性胆道疾患外科治療</p>
                                <p class="mb15">タイトル　急性胆嚢炎の外科治療</p>
                              ';
                  }else if($m_seminar_group_id == 5){
                    $output .='<p>分野／領域　放射線診断・治療</p>
                                <p class="mb15">タイトル　胆道MRIのエッセンス</p>
                              ';
                  }else if($m_seminar_group_id == 6){
                    $output .='<p>分野／領域　病理診断</p>
                               <p>タイトル</p>
                                <p class="mb15">胆管内乳頭状腫瘍 (IPNB)の基本事項の確認と臨床病理学的意義</p>
                              ';
                  }

                  $output .='<p class="mb100">⽒ &emsp;  '.$full_name.'</p>

                  <table style="width:100%;">
                      <tbody>
                         <tr>
                            <td style="width:15%;"></td>
                            <td class="text-left"><img width="150" src=" '.asset('img/jba.png').' "> </td>
                            <td style="width:80%; font-size:31px;" class="text-right"><p>⼀般社団法⼈ ⽇本胆道学会</p><br><p>理事⻑ 海野 倫明</p></td>
                            <td style="width:5%;"></td>
                            <td class="text-right"><img width="150" src="'. asset('img/qr_certificate.png') .'"></td>
                         </tr>
                      </tbody>
                   </table>
                </div>
              </div>
           </body>
        </html>';

    return $output;
}