<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\ManagementCompany; 

class DefaultEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $management_company = get_management_company_detail();
        
        if(empty($data['email_type'])){
            $data['email_type']='Default Email';
        }

        $this->data = $data;
        
        if($management_company){
            $this->data['support_name'] = $management_company->support_name;
            $this->data['support_mail_address'] = $management_company->support_mail_address;
        }
        
    }
    
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))->subject($this->data['subject'])->markdown('emails.default_email')->with(['data' => $this->data]);
    }
}
