<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\ManagementCompany;

class VerifyRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $management_company = ManagementCompany::first();

        $this->data = $data;

        if($management_company){
            $this->data['support_name'] = $management_company->support_name;
            $this->data['support_mail_address'] = $management_company->support_mail_address;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))->subject($this->data['subject'])->markdown('emails.verify_registration')->with(['data' => $this->data]);
    }
}
