<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seminar extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'm_seminars';

    protected $fillable = [
        'm_seminar_group_id',
        'category_no',
        'category_name',
        'seminar_title',
        'seminar_hours',
        'start_at',
        'end_at',
        'capacity',
        'seminar_date',
        'reception_open_at',
        'reception_close_at',
        'teacher_name',
        'teacher_affiliation1',
        'teacher_affiliation2',
        'teacher_affiliation3',
        'place',
        'movie_url',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $dates = [
        'start_at',
        'end_at',
        'reception_open_at',
        'reception_close_at',

    ];
    
    public function seminarGroup(){
        return $this->belongsTo('App\Models\SeminarGroup', 'm_seminar_group_id', 'id');
    }

    public function exams(){
        return $this->hasMany('App\Models\Exam', 'm_seminar_id', 'id');
    }

    public function video()
    {
        return $this->hasOne(SeminarVideo::class, 'm_seminar_id');
    }

}
