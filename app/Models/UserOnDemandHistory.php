<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOnDemandHistory extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'user_ondemand_histories';

    protected $fillable = [
        'user_id',
        'm_seminar_id',
        'attend_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

}
