<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $primaryKey = 'id';

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'l_name',
        'f_name',
        'l_name_kana',
        'f_name_kana',
        'certificate_id1',
        'certificate_id2',
        'registration_id',
        'mail_address',
        'hospital_name',
        'devision_name',
        'tel',
        'birthday',
        'password',
        'email_check_at',
        'email_verification_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function getHasPasswordAttribute()
    {
        return ! empty($this->attributes['password']);
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    protected $appends = ['fullname','fullname_kana'];

    public function getFullnameAttribute(){
        return $this->l_name.' '.$this->f_name;
    }

    public function getFullnameKanaAttribute(){
        return $this->l_name_kana.' '.$this->f_name_kana;
    }


}
