<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarVideo extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    protected $table = 'seminar_videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'm_seminar_id',
        'video_open_at',
        'video_close_at',
        'm3u8file_path',
        'keep_original_flag',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */

    public function getM3u8filePathAttribute($value){
        if (!empty($value)) {
            // $url = env('APP_SEMINAR_MANAGEMENT_URL').'video/'.$this->m_seminar_id.'/video.m3u8';
            $url = asset('video/'.$this->m_seminar_id.'/video.m3u8');
            return $url;
        }else{
            $url = null;
        }
    }

}
