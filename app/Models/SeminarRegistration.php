<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarRegistration extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'seminar_registrations';

    protected $fillable = [
        'm_seminar_group_id',
        'm_seminar_id',
        'user_id',
        'qr_code',
        'registration_at',
        'canceled_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];


    public function seminar(){
        return $this->belongsTo('App\Models\Seminar','m_seminar_id');
    }

    public function userExams(){
        return $this->hasMany('App\Models\UserExam', 'seminar_registration_id', 'id');
    }
}
