<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarCertificate extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'seminar_certificates';

    protected $fillable = [
        'm_seminar_group_id',
        'm_seminar_id',
        'user_id',
        'user_exam_id1',
        'user_exam_id2',
        'user_exam_id3',
        'correct_answer_count',
        'certificate_file',
        'seminar_type',
        'exam_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function seminarGroup(){
        return $this->belongsTo('App\Models\SeminarGroup','m_seminar_group_id');
    }

}
