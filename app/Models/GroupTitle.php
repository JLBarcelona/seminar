<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupTitle extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    
    protected $table = 'm_group_title';

    protected $fillable = [
        'title_name',
    ];

}
