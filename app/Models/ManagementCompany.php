<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagementCompany extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'm_management_companies';

    protected $fillable = [
        'support_mail_address',
        'support_name',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

}
