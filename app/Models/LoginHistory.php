<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    
    protected $table = 'login_histories';

    protected $fillable = [
        'login_user_id',
        'start_on',
        'end_on',
        'created_at',
        'updated_at'
    ];

}
