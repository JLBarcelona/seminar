<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Models\ManagementCompany;
use App\Models\CompanyRegistration;
use App\Models\LoginHistory;
use App\Models\Seminar;
use App\Models\SeminarRegistration;
use App\Models\SeminarCertificate;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

function save_auth_logs($auth_id, $type = 'login'){

	$s_id = session()->getId();
	if ($type == 'login') {
		$history = new LoginHistory;
		$history->login_user_id = $auth_id;
		$history->session_id = $s_id;
		$history->start_on = now();
		if ($history->save()) {
				return true;
		}
	}elseif ($type == 'logout') {
		$has_login = LoginHistory::where('login_user_id', $auth_id)->where('session_id',$s_id)->first();
		if ($has_login) {
			$has_login->end_on = now();
			if ($has_login->save()) {
				return true;
			}
		}
	}
}

function get_management_company_detail()
{
	$query = ManagementCompany::selectRaw('support_mail_address, support_name')->get()->first();
	return $query;
}


function getDay($date){
    $year = date('Y',strtotime($date));
    $month = date('m',strtotime($date));
    $day = date('d',strtotime($date));
    $array = [
        '日','月','火','水','木','金','土'
    ];
    $dayNum = date('w',strtotime($date));
    $Newdate = $year.'年'.$month.'月'.$day.'日'.'('.$array[$dayNum].')';
    return  $Newdate;
}

function japan_date($date,$mode=''){
	if(!empty($date)){
		if(empty($mode)){
			return date("Y年n月j日", strtotime($date));
		}elseif($mode=='md'){
			return date("n月j日", strtotime($date));
		}

	}else{
		return "";
	}
}

function getAge($bday){
    if(!empty($bday)){
        $date = new DateTime($bday);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y.'歳';
    }else{
        return "";
    }
}

function getGender($gender){
    $array = [
        1 => "男性",
        2 => "女性"];
    return $array[$gender];
}

function getWeek($date){
  $array = ['日','月','火','水','木','金','土'];
  $dayNum = date('w',strtotime($date));
  return  ' ('.$array[$dayNum].')';
}

function getJapanDateWeek($date){
  $japan_week = ['日','月','火','水','木','金','土'];
  $dayNum = date('w',strtotime($date));
  if(!empty($date)){
    return date("Y年m月d日", strtotime($date)) . ' ('.$japan_week[$dayNum].')';
  }else{
    return "";
  }
}

function capacity_left($id){
  $seminar = Seminar::find($id);
  $seminar_count = SeminarRegistration::where('m_seminar_id',$id)->whereNull('canceled_at')->count();
  return $capacity_left = ($seminar_count / $seminar->capacity);
}

function check_seminar_certificate($seminar_id){
  $user_id = Auth::user()->id;

  $certificate = SeminarCertificate::where(['user_id'=>$user_id, 'm_seminar_id' => $seminar_id])->first();

  if($certificate){
    return true;
  }else{
    return false;
  }

}