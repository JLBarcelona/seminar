<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

use App\Models\User;
use App\Models\SeminarCertificate;

class SeminarCertificateController extends Controller
{
    public function pdfCertificate1(Request $request)
    {

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4',
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => 10,
            'margin_bottom' => 10,
            'margin_header' => 0,
            'margin_footer' => 0,
            'default_font_size' => '14',
            'default_font'      => '',
            'display_mode' => 'fullpage',
            'mode' => 'ja+aCJK',
        ]);

        $mpdf->WriteHTML(certificatePdf1($request));
        $mpdf->Output(base_path().'/public/pdf/'.$pdf_name,'F'); 

    }

}