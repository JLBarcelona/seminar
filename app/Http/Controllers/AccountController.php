<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;

use App\Models\User;
use App\Models\SeminarGroup;
use App\Models\GroupTitle;
use App\Models\SeminarCertificate;

class AccountController extends Controller
{
    public function index(){
        $user = Auth::user();
        $today = date('Y-m-d');
        $seminar_groups = SeminarGroup::select('id','group_name')->where('registration_from','<=',$today)->where('registration_to','>=',$today)->get();

        $v_seminar_query = DB::table('V_SEMINAR_GROUP')
            ->distinct()->select('m_seminar_group_id','group_name')
            ->join('seminar_registrations', 'V_SEMINAR_GROUP.id', '=', 'seminar_registrations.m_seminar_group_id')
            ->where('V_SEMINAR_GROUP.display_end','>=',$today);

        if(!$seminar_groups->isEmpty()){
            $v_seminar_query->whereIn('m_seminar_group_id',$seminar_groups->pluck('id'));
        }

        $v_seminar_group = $v_seminar_query->where('seminar_registrations.user_id',$user->id)->whereNull('seminar_registrations.canceled_at')->get();

        $checker = [];

        if(!$v_seminar_group->isEmpty()){

            foreach ($seminar_groups->toArray() as $row) {

                $data['id'] =  $row['id'];
                $data['group_name'] =  $row['group_name'];
                if ($v_seminar_group->contains('m_seminar_group_id', $row['id'])){
                    $data['is_registered'] = true;
                }else{
                    $data['is_registered'] = false;
                }
                $checker[] = $data;
            }
        }
        $seminar_group_title = GroupTitle::get();
        $show_user_url = route('account.show', ['id' => $user->id]);

        $course_taken = SeminarCertificate::with('seminarGroup')->where('user_id',$user->id)->get();

        return view('Account.index',compact('user','v_seminar_group','show_user_url','seminar_groups','checker','seminar_group_title','course_taken'));

    }

    public function showUserAccount(Request $request,$id){
        $user = User::find($id);
    	return view('Account.show_user_account',compact('user'));
    }

    public function editUserAccount(Request $request,$id){
        $user = User::find($id);
    	return view('Account.edit_user_account',compact('user'));
    }

    public function update(Request $request, $id){
        $user = User::find($id);

        $validator = Validator::make($request->all(), [
            'l_name' => 'required',
            'f_name' => 'required',
            'l_name_kana' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u',
            'f_name_kana' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u',
            'certificate_id1' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u|max:1',
            'certificate_id2' => 'required|max:4',
            'mail_address' => 'required|email|unique:users,mail_address,'.$user->id,
            'hospital_name' => 'required',
            'devision_name' => 'required',
            'tel' => 'required|regex:/^\d{2,4}-\d{2,6}-\d{2,6}$/|max:16',
            'birthday' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

            $update = User::where('id', $user->id)->update([
                'l_name'=>$request->l_name,
                'f_name'=>$request->f_name,
                'l_name_kana'=>$request->l_name_kana,
                'f_name_kana'=>$request->f_name_kana,
                'certificate_id1'=>$request->certificate_id1,
                'certificate_id2'=>$request->certificate_id2,
                'registration_id'=>$request->registration_id,
                'mail_address'=>$request->mail_address,
                'hospital_name'=>$request->hospital_name,
                'devision_name'=>$request->devision_name,
                'tel'=>$request->tel,
                'birthday'=>$request->birthday]);

            if($update){
                return response()->json(['status' => true, 'redirect' => route('account.show', ['id' => $user->id])]);
            }
        }
    }

    public function uploadCertificate(Request $request){

       $user = Auth::user();

       $validator = Validator::make($request->all(), [
            'upload_file' => 'required'
       ]); 

        if ($validator->fails()) {
           return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $file_name = $request->file('upload_file');
            $file_upload = $this->upload_document($file_name, $user->id);

            if($file_upload){
                $user_update = User::where('id',$user->id)->update(['paid' => 1, 'registration_file' => $file_upload]);
                if($user_update){
                    return response()->json(['status' => true, 'redirect' => route('SeminarVideo.index')]);
                }else{
                    return response()->json(['status' => false, 'message' => 'Error on uploading']);
                }
            }
        }
    }

    public function upload_document($file_content, $id){
        if (!empty($file_content)) {
            $file_extension = $file_content->extension();
            $org_filename = $file_content->getClientOriginalName();
            $file_name  = $id.'_'.$org_filename;
            if(Storage::disk('upload_document')->put($file_name, file_get_contents($file_content)) ) {
            }
            return $file_name;
        }
    }

}
