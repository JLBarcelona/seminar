<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Process\Process;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;

use App\Models\SeminarVideo;
use App\Models\UserOnDemandHistory;
use App\Models\Seminar;
use App\Models\SeminarCertificate;

use PhpOffice\PhpWord;

class SeminarVideoController extends Controller
{   

    public function course_result($seminar_id,$seminar_type){

      $user = Auth::user();
      $user_id = $user->id;
      $certificate = SeminarCertificate::where(['user_id'=>$user_id, 'm_seminar_id'=>$seminar_id, 'seminar_type' => $seminar_type])->first();
      $seminar = Seminar::with('seminarGroup')->where('id',$seminar_id)->first();
      $data = [];
      $exam_status = "不合格";
      $with_certificate = false;
      $certificate_file = '';
      $output_file = '';
      if($certificate){
        $exam_status = ($certificate->correct_answer_count > 1) ? "合格" : "不合格";
        $with_certificate = ($certificate->correct_answer_count > 1) ? true : false;
        $certificate_file = $certificate->certificate_file;
        $output_file = '受講証明書_'. $seminar->category_name.'_'.$user->l_name.''.$user->f_name.'_';
      }
      if($seminar){

        // convert certificate
        exec('export HOME=/tmp && lowriter --convert-to pdf --outdir '.public_path().'/certificate_pdf '.public_path() .'/certificate_word/'.$certificate_file.'.docx');

        $data = ['seminar_id'=>$seminar_id, 'group_name' => $seminar->seminarGroup->group_name, 'exam_status' => $exam_status, 'with_certificate' => $with_certificate,'certificate_file' => $certificate_file.'.pdf', 'output_file' => $output_file];
        return view('SeminarVideo.course_result',compact('data','user'));
      }else{
        abort(404);
      }

    }

    public function index(){

      $user_id = Auth::user()->id;

      $today_date = Carbon::now();

      $seminarVideos = Seminar::selectRaw('m_seminars.id as seminar_id,seminar_videos.id as seminar_video_id, m_seminar_groups.group_name')
                            ->join('seminar_videos', 'seminar_videos.m_seminar_id', '=', 'm_seminars.id')
                            ->join('m_seminar_groups', 'm_seminar_groups.id', '=', 'm_seminars.m_seminar_group_id')
                            ->where('seminar_videos.video_open_at','<=',$today_date)->where('seminar_videos.video_close_at','>=',$today_date)
                            ->orderBy('m_seminar_id')->get();

      $videos_attend = UserOnDemandHistory::where('user_id',$user_id)->whereNotNull('attend_at')->get();

      if(!$seminarVideos->isEmpty()){
          $videos_attend->whereIn('m_seminar_group_id',$seminarVideos->pluck('id'));
      }

      $checker = [];

      if(!$videos_attend->isEmpty()){

          foreach ($seminarVideos->toArray() as $row) {

              $data['seminar_id'] =  $row['seminar_id'];
              $data['group_name'] =  $row['group_name'];
              if ($videos_attend->contains('m_seminar_id', $row['seminar_id'])){
                  $data['is_registered'] = true;
              }else{
                  $data['is_registered'] = false;
              }
              $checker[] = $data;
          }

      }

      if(Auth::user()->paid == 1){
        return view('SeminarVideo.index', compact('seminarVideos','checker'));
      }else{
        return view('Account.upload_participation_certificate');
      }
 
    }

    public function start_video($seminar_id){
      $user_id = Auth::user()->id;
      $with_certificate = SeminarCertificate::where(['user_id'=>$user_id, 'm_seminar_id'=>$seminar_id, 'seminar_type' => 'video'])->first();
      
      $seminar_detail = Seminar::selectRaw('m_seminar_groups.group_name,seminar_videos.video_times')
                        ->join('m_seminar_groups', 'm_seminar_groups.id', '=', 'm_seminars.m_seminar_group_id')
                        ->join('seminar_videos', 'seminar_videos.m_seminar_id', '=' , 'm_seminars.id')
                        ->find($seminar_id);
      // if($with_certificate){
      //   abort(404);
      // }else{
      //   if($seminar_detail){
      //     return view('SeminarVideo.start_video', compact('seminar_detail','seminar_id'));
      //   }else{
      //     abort(404);
      //   }
      // }
        return view('SeminarVideo.start_video', compact('seminar_detail','seminar_id'));


    }

    public function save_video($seminar_id){
      $user = Auth::user();
      $has_user_demand = UserOnDemandHistory::where('m_seminar_id', $seminar_id)->where('user_id', $user->id);

      if ($has_user_demand->count() > 0) {
          $user_demand_history = $has_user_demand->first();
          return redirect()->route('SeminarVideo.play.video', ['user_ondemand_history_id' => $user_demand_history->id]);
      }else{
        $data = new UserOnDemandHistory;
        $data->user_id = Auth::user()->id;
        $data->m_seminar_id = $seminar_id;
        $data->attend_at = Carbon::now();

        if($data->save()){
          return redirect()->route('SeminarVideo.play.video', ['user_ondemand_history_id' => $data->id]);
        }else{
          return Redirect::to(url()->previous());
        }
      }
    }

    public function play_video($user_ondemand_history_id){

        $user_id = Auth::user()->id;
        $user_ondemand_history = UserOnDemandHistory::find($user_ondemand_history_id);
        $is_exam = true;
        if($user_ondemand_history){
          $seminar_detail = Seminar::with(
            ["seminarGroup","video" ,"exams" => function($q){ 
              $q->inRandomOrder()->limit(3);
            }]
          )
          ->whereHas('seminarGroup')
          ->whereHas('video')
          ->whereHas('exams')
          ->find($user_ondemand_history->m_seminar_id);  

          if($seminar_detail){
            $with_certificate = SeminarCertificate::where(['user_id'=>$user_id, 'm_seminar_id'=>$user_ondemand_history->m_seminar_id, 'seminar_type' => 'video'])->first();
            if($with_certificate){
                $redirect = route('SeminarVideo.course.result', ['seminar_id' => $user_ondemand_history->m_seminar_id, 'seminar_type' => 'video']);                            
            }else{
                $redirect = route('SeminarVideo.exam', [$user_ondemand_history->id, 1]);
            }

              return view('SeminarVideo.play_video', compact('user_ondemand_history','seminar_detail', 'redirect'));

          }else{
            abort(404);
          }
        }
        abort(404);
    }

    public function exam($user_ondemand_history_id, $video_finished = 0){

        $user_id = Auth::user()->id;
        $user_ondemand_history = UserOnDemandHistory::find($user_ondemand_history_id);
        if($user_ondemand_history){
          $seminar_detail = Seminar::with(
            ["seminarGroup","video" ,"exams" => function($q){ 
              $q->whereNotNull('exam_question')->inRandomOrder()->limit(3);  
            }]
          )
          ->whereHas('seminarGroup')
          ->whereHas('video')
          ->whereHas('exams')
          ->find($user_ondemand_history->m_seminar_id);  

          $no_seminar_exam = false;

          if($seminar_detail->exams->isEmpty()){
            $no_seminar_exam = true;
            return view('SeminarVideo.exam', compact('user_ondemand_history','seminar_detail','no_seminar_exam'));
          }

          if($seminar_detail){
            $with_certificate = SeminarCertificate::where(['user_id'=>$user_id, 'm_seminar_id'=>$user_ondemand_history->m_seminar_id, 'seminar_type' => 'video'])->first();
            if($with_certificate){
              abort(404);
            }else{
              if($video_finished == 1){
                return view('SeminarVideo.exam', compact('user_ondemand_history','seminar_detail','no_seminar_exam'));
              }else{
                abort(404);
              }
            }
          }else{
            abort(404);
          }
        }
        abort(404);
    }



}