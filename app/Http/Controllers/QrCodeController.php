<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SeminarRegistration;
use App\Models\UserSeminarStatus;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use DB;
use Mail;
use Hash;
use Auth;

class QrCodeController extends Controller
{
    public function index($id, $seminar_id)
    {
       $user = Auth::user();
       $today = now();
        $has_seminar = SeminarRegistration::whereHas('seminar', function($seminars) use($today){
        $seminars->whereDate('reception_open_at', '<=', $today->format('Y-m-d'))->whereTime('reception_open_at', '<=', $today->toTimeString());
       })->where('id', $id)->where('user_id', $user->id)->whereNull('canceled_at');

       if ($has_seminar->count() > 0) {
       	$seminar = $has_seminar->first();
        return view('QrCode.index', compact('seminar'));
       }else{
       	return redirect(route('seminar.user.detail', ['id' => $id]));
       }
    }

    public function checkSeminar($id){
      $has_reception = UserSeminarStatus::where('seminar_registration_id', $id);
      if ($has_reception->count() > 0) {
        $reception = $has_reception->first();
        $data = [];

        if (!empty($reception->reception_in_err) || !empty($reception->reception_out_err)) {
          return response()->json(['status' => true, 'redirect' => route('qrcode.error.seminar', [$id])]);
        }else{
          if (!empty($reception->reception_in_at) && !empty($reception->reception_out_at) && !empty($reception->exam_answered_at)) {
            return response()->json(['status' => true, 'redirect' => route('qrcode.out.seminar', [$id])]);
          }elseif (!empty($reception->reception_in_at) && empty($reception->exam_answered_at)) {
            return response()->json(['status' => true, 'redirect' => route('qrcode.in.seminar', [$id])]);
          }
        }
      }else{
      return response()->json(['status' => false, 'message' => 'Stay Here..']);
      }
    }


    public function errorSeminar($id){
       $user = Auth::user();
       $seminar = SeminarRegistration::with(['seminar'])->where('id', $id)->where('user_id', $user->id)->first();
       return view('QrCode.err_status', compact('seminar'));
    }

    public function inSeminar($id){
       $user = Auth::user();
      $seminar = SeminarRegistration::with(['seminar'])->where('id', $id)->where('user_id', $user->id)->first();
      return view('QrCode.in_status', compact('seminar'));
    }

    public function outSeminar($id){
       $user = Auth::user();
      $seminar = SeminarRegistration::with(['seminar'])->where('id', $id)->where('user_id', $user->id)->first();
      return view('QrCode.out_status', compact('seminar'));
    }

}
