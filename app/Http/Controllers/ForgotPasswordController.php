<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB; 
use Carbon\Carbon; 
use App\Models\User; 
use Mail; 
use Hash;
use Illuminate\Support\Str;
use Illuminate\Mail\Mailable;
use App\Mail\DefaultEmail;

class ForgotPasswordController extends Controller
{
    public function index()
    {
       return view('Auth.ResetPassword.index');
    }

    public function store(Request $request)
    {

        $rules = [ 'mail_address' => 'required|email|exists:users'];
        $message = ['mail_address.required' => 'メールアドレスフィールドは必須です。','mail_address.exists' => '選択したメールアドレスが無効です。','mail_address.email' => 'メールアドレスの書式が正しくありません'];

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $token = Str::random(64);

            $url = route('auth.reset.password.get', ['token' => $token]);
            
            $update = User::where('mail_address', $request->mail_address)
            ->update(['password_reset_parameter' => $token]);
            
            $user_info = User::where('mail_address', $request->mail_address)->first();

            if($update){
                $data = [
                        'url' => $url, 
                        'title' => '会社の +Seminar パスワードリセット',
                        'subject' => '日本胆道学会認定医養成講座：パスワードリセットURLのお知らせ。',
                        'email_type' => 'password_reset',
                        'full_name' => $user_info->l_name . ' ' . $user_info->f_name,
                        ];

                Mail::to($request->mail_address)->send(new DefaultEmail($data)); 

                return response()->json(['status' => true, 'message' => 'メールリセットパスワードが正常に送信されました。', 'mail_address' => $request->mail_address, 
                    'url' => route('auth.reset.password.sent')

                ]);
            }
           
        }

       
    }

    public function showResetPasswordForm($token) { 

        $user_data = User::select('id','mail_address','password_reset_parameter')->where('password_reset_parameter',$token)->get()->first();
        return view("Auth.ResetPassword.reset_password", compact('user_data'));
    }

    public function submitResetPasswordForm(Request $request)
    {

        $rules = [ 'mail_address' => 'required|email|exists:users', 
                    'password' => 'required|min:8|max:8|regex:/^[a-zA-Z0-9]+$/u',
                    'password_confirmation' => 'required|min:8|max:8|same:password'];
        $message = [
            'mail_address.required' => 'メールアドレスフィールドは必須です。',
            'mail_address.exists' => '選択したメールアドレスが無効です。',
            'password.required' => '必須入力項目です',
            'password_confirmation.same' => '同じパスワードを入力してください',
            'password.regex' => '英数字8桁（半角英字、数字）'];

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

            $user = User::find($request->id);
            $user->password_reset_parameter = null;
            $user->password =$request->password;

            if($user->save()){
                return response()->json(['status' => true, 'message' => 'メールリセットパスワードが正常に送信されました。', 'url' => route('auth.reset.password.success')]);
            }else{
                return response()->json(['status' => false, 'message' => 'failed']);
            }
         
           
        }

    }

    public function resetSuccess(){
		return view('Auth.ResetPassword.reset_password_success');
	}

    public function sentResetPasswordPage(){
        return view('Auth.ResetPassword.reset_password_sent');
    }

}
