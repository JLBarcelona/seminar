<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;



use App\Models\User;
use App\Models\Seminar;
use App\Models\SeminarGroup;
use App\Models\SeminarRegistration;
use App\Models\UserExam;
use App\Models\UserSeminarStatus;
use App\Models\UserOnDemandHistory;
use App\Models\SeminarCertificate;

use PDF;
use PhpOffice\PhpWord;

class ExamController extends Controller
{

    public function index($seminar_registration_id){

        $user = Auth::user();

        if($seminar_registration_id){

            $seminar_registration = SeminarRegistration::where('id',$seminar_registration_id)->where('user_id',$user->id)->whereNull('canceled_at')->first();

            if($seminar_registration){
                $seminar = Seminar::has('exams')->with(["exams" => function($q){ 
                    $q->where('seminar_use',1)->orderBy('question_no');
                }])->find($seminar_registration->m_seminar_id);
               
                if($seminar){
                    $exam_ids = $seminar->exams->pluck('id');

                    //check if user has already answered the exam.
                    $user_exams = UserExam::whereIn('exam_id',$exam_ids)->where('seminar_registration_id',$seminar_registration->id)->where('user_id',$user->id)->get();

                    if(!$user_exams->isEmpty()){
                        return redirect()->route('seminar.user.detail', ['id' => $seminar_registration->id]);
                    }

                    $user_seminar_status = UserSeminarStatus::where('seminar_registration_id',$seminar_registration->id)->first();

                    if(!$user_seminar_status){
                        return redirect()->route('seminar.user.detail', ['id' => $seminar_registration->id]);
                    }

                    return view('Exam.index',compact('seminar_registration','seminar','user'));
                }else{ //seminar has no exams or seminar doesn't exists
                    return redirect()->route('seminar.user.detail', ['id' => $seminar_registration->id]); 
                }
               
            }else{ //seminar has no exams or seminar doesn't exists
                    return redirect()->route('seminar.user.detail', ['id' => $seminar_registration_id]); 
                }
            
          
        }

    }

    public function store(Request $request){

        $user = Auth::user();

        $rules=[];

        //validate/get the user registration and seminar exams first
        if($request->filled('seminar_registration_id')){
            $seminar_registration = SeminarRegistration::where('id',$request->seminar_registration_id)->where('user_id',$user->id)->whereNull('canceled_at')->first();
            if($seminar_registration){

                //validate user_seminar_status
                $user_seminar_status = UserSeminarStatus::where('seminar_registration_id',$request->seminar_registration_id)->first();

                if(!$user_seminar_status){
                    return redirect()->route('seminar.user.detail', ['id' => $seminar_registration->id]);
                }

                if($user_seminar_status && $user_seminar_status->exam_answered_at!=null){
                    return response()->json([
                        'status' => false, 
                        'type' => 'answered',
                        'message' => 'fail', 
                        'redirect' => route('seminar.user.detail', ['id' => $seminar_registration->id])
                    ]);
                }

                $seminar = Seminar::has('exams')->with(["exams" => function($q){ 
                    $q->where('seminar_use',1)->orderBy('question_no');
                    }])->find($seminar_registration->m_seminar_id);
                if($seminar){
                  
                    foreach($seminar->exams as $key => $eq){
                        $rules["q-".$eq->id] = 'required';
                    }

                    $validator = \Validator::make($request->all(),$rules,[]);

                    if ($validator->fails()) {
                        return response()->json(['status' => false, 'type' => 'answer_all','error' => $validator->errors()]);
                    }
                    
                    //build insert data
                    $data = array();
                    foreach($seminar->exams as $key => $eq){

                        $field['user_id'] = $user->id;
                        $field['seminar_registration_id'] = $seminar_registration->id;
                        $field['exam_id'] = $eq->id;
                        $field['answer'] = $request->input('q-'.$eq->id);
                        $field['correct'] = ($eq->answer == $request->input('q-'.$eq->id) ? 1 : 0);
                        $field['created_at'] = now();
                        $data[] = $field;

                    }
                    //insert records
                    UserExam::insert($data);

                    $user_exam_query = UserExam::
                    where('seminar_registration_id',$seminar_registration->id);
                    
                    $user_exams = $user_exam_query->get();
                    if(!$user_exams->isEmpty()){

                        $user_exams_correct = $user_exam_query->where('correct',1)->count();
                
                        //SeminarCertificate
                        if($user_exams_correct > 1){

                            //generate certificate pdf
                            $file_name = 'JBA'.$seminar->m_seminar_group_id.'_'.$user->id.'_seminar';
                            $full_name = $user->l_name . " " . $user->f_name;
                            $this->convertWordToPDF($full_name,$seminar->format_url,$file_name);

                            $sem_cert_arry = [
                                'm_seminar_group_id' => $seminar->m_seminar_group_id,
                                'm_seminar_id' => $seminar_registration->m_seminar_id,
                                'user_id' => $user->id,
                                'user_exam_id1' => '',
                                'user_exam_id2' => '',
                                'user_exam_id3' => '',
                                'correct_answer_count' => $user_exams_correct,
                                'certificate_file' => $file_name,
                                'seminar_type' => 'seminar',
                                'exam_at' => now(),
                            ];

                            foreach($user_exams as $key => $exam){
                                $sem_cert_arry['user_exam_id'.$key+1] = $exam->id;
                            }

                            $seminar_certificate = SeminarCertificate::create($sem_cert_arry);
                        }

                    }

                    UserSeminarStatus::where('seminar_registration_id',$seminar_registration->id)->update(['exam_answered_at' => now()]);
                    return response()->json([
                        'status' => true, 
                        'message' => 'success', 
                        'redirect' => route('seminar_exam.success', ['seminar_registration_id' => $seminar_registration->id])
                    ]);

                }
            }
        }

    }

    public function userOnDemandExamstore(Request $request){

        $user = Auth::user();

        $rules=[];

        if($request->filled('user_ondemand_history_id')){
            $user_ondemand_history = UserOnDemandHistory::where('id',$request->user_ondemand_history_id)->where('user_id',$user->id)->first();
            if($user_ondemand_history){
  
                $seminar = Seminar::has('exams')->with(["exams" => function($q) use($request){ 
                        $q->whereIn('id',$request->exam_id)->orderBy('question_no');
                    }])->find($user_ondemand_history->m_seminar_id);
                if($seminar){
                  
                    foreach($seminar->exams as $key => $eq){
                        $rules["q-".$eq->id] = 'required';
                    }

                    $validator = \Validator::make($request->all(),$rules,[]);

                    if ($validator->fails()) {
                        return response()->json(['status' => false, 'type' => 'answer_all','error' => $validator->errors()]);
                    }
                    
                    //build insert data
                    $data = array();
                    foreach($seminar->exams as $key => $eq){

                        $field['user_id'] = $user->id;
                        $field['m_seminar_id'] = $user_ondemand_history->m_seminar_id;
                        $field['user_ondemand_history_id'] = $user_ondemand_history->id;
                        $field['exam_id'] = $eq->id;
                        $field['answer'] = $request->input('q-'.$eq->id);
                        $field['correct'] = ($eq->answer == $request->input('q-'.$eq->id) ? 1 : 0);
                        $field['created_at'] = now();
                        $data[] = $field;

                    }
                    
                    DB::beginTransaction();

                    // insert exam records
                    UserExam::insert($data);

                    $user_exam_query = UserExam::
                    where('user_ondemand_history_id',$user_ondemand_history->id)
                    ->where('m_seminar_id',$user_ondemand_history->m_seminar_id);
                    
                    $user_exams = $user_exam_query->get();
                    if(!$user_exams->isEmpty()){

                        $user_exams_correct = $user_exam_query->where('correct',1)->count();
                
                        //SeminarCertificate
                        if($user_exams_correct > 1){

                            //generate certificate pdf
                            $file_name = 'JBA'.$seminar->m_seminar_group_id.'_'.$user->id.'_video';
                            $full_name = $user->l_name . " " . $user->f_name;
                            $this->convertWordToPDF($full_name,$seminar->format_url,$file_name);

                            $sem_cert_arry = [
                                'm_seminar_group_id' => $seminar->m_seminar_group_id,
                                'm_seminar_id' => $user_ondemand_history->m_seminar_id,
                                'user_id' => $user->id,
                                'user_exam_id1' => '',
                                'user_exam_id2' => '',
                                'user_exam_id3' => '',
                                'correct_answer_count' => $user_exams_correct,
                                'certificate_file' => $file_name,
                                'seminar_type' => 'video',
                                'exam_at' => now(),
                            ];

                            foreach($user_exams as $key => $exam){
                                $sem_cert_arry['user_exam_id'.$key+1] = $exam->id;
                            }

                            $seminar_certificate = SeminarCertificate::create($sem_cert_arry);
                        }

                    }
    
                    DB::commit();
                    
                    return response()->json([
                        'status' => true, 
                        'message' => 'success', 
                        'redirect' => route('SeminarVideo.course.result', ['seminar_id' => $user_ondemand_history->m_seminar_id, 'seminar_type' => 'video'])
                    ]);

                }
            }
        }

    }

    function generate_certificate($full_name,$file_name,$m_seminar_group_id){

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4',
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
            'default_font_size' => '14',
            'default_font'      => '',
            'display_mode' => 'fullpage',
            'mode' => 'ja+aCJK',
        ]);

        $mpdf->WriteHTML(certificatePdf($full_name,$m_seminar_group_id));
    }

    public function success($seminar_registration_id){

        $user = Auth::user();

        if($seminar_registration_id){

            $seminar_registration = SeminarRegistration::where('id',$seminar_registration_id)->where('user_id',$user->id)->whereNull('canceled_at')->first();
            if($seminar_registration){
                $seminar = Seminar::has('exams')->with('exams')->find($seminar_registration->m_seminar_id);
                return view('Exam.success',compact('seminar_registration','seminar','user'));
            } 
            
        }
    }

    public function convertWordToPDF($full_name, $format_url,  $file_name)
    {   

        /*@ Reading doc file */
        $template = new \PhpOffice\PhpWord\TemplateProcessor((public_path('certificate/'.$format_url)));
 
        /*@ Replacing variables in doc file */
        $template->setValue('name', $full_name);

        /*@ Save Temporary Word File With New Name */
        $saveDocPath = public_path('certificate_word/'.$file_name.'.docx');
        // $exitCode = Artisan::call('lowriter --convert-to pdf ' .  public_path('certificate_word/'.$file_name))
        $template->saveAs($saveDocPath);

    }

}
