<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use App\Models\User;
use Validator;


class AuthController extends Controller
{


	public function index(){
		if (Auth::check()) {
			return redirect(route('account.index'));
		}else{
			return view('Auth.index');
		}
	}

	public function authenticate(Request $request){
		 $validator = Validator::make($request->all(), [
            'mail_address' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

        	$credentials = $request->validate([
	            'mail_address' => ['required', 'email'],
	            'password' => ['required'],
	        ]);

	        if (Auth::attempt($credentials)) {
	            $request->session()->regenerate();
	            $user = Auth::user();

	            if(empty($user->email_check_at) && !empty($user->email_verification_token)) {
            		Session::flush();
					Auth::logout();
		            return response()->json(['status' => true, 'redirect' => route('login') . "?authenticate=false"]);
            	}else{
		         	return response()->json(['status' => true, 'redirect' => route('account.index')]);
            	}

	        }else{
	        	$validator->errors()->add('password','パスワードが違います');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	        }
        }
	}

	public function logout()
    {
		$user = Auth::user();
		Session::flush();
    	Auth::logout();
		return redirect('login');
		// if(save_auth_logs($user->id,'logout')) {
			
		// }
    }
}