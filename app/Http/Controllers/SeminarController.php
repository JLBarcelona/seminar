<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Seminar;
use App\Models\SeminarGroup;
use App\Models\SeminarRegistration;
use App\Models\UserSeminarStatus;
use App\Models\UserExam;

class SeminarController extends Controller
{
    public function index($seminar_group_id){

       //seminar list
       if($seminar_group_id){
            $today_date = Carbon::now();
            $seminar_group = SeminarGroup::with(["seminars" => function($q){
                    $q->orderBy('category_no');
                }])->where('id',$seminar_group_id)->first();
            
            //return $seminar_group;
            return view('Seminar.index',compact('seminar_group','today_date'));
       }
    }

    public function show($id){ //seminar detail
      $user = Auth::user();
      $seminar = Seminar::find($id);
      $capacity_left = capacity_left($id);
      //check if the user already register on the event
      $with_registration = SeminarRegistration::where('user_id',$user->id)->where('m_seminar_id',$id)->whereNull('canceled_at');
      return view('Seminar.detail' ,compact('seminar','capacity_left','with_registration'));
    }

    public function register(Request $request){ //seminar registration
      $user = Auth::user();
      $group_id = $request->get('group_id');
      $seminar_id = $request->get('seminar_id');

      $capacity = capacity_left($seminar_id);

      $check_duplicate = SeminarRegistration::where('user_id',$user->id)->where('m_seminar_group_id',$group_id)->where('m_seminar_id',$seminar_id)->whereNull('canceled_at')->count();

      if($check_duplicate > 0){
        return response()->json(['status' => false, 'type' => 'duplicate']);
      }else{
        if($capacity >= 1){
          return response()->json(['status' => false, 'type' => 'capacity_full', 'redirect' => route('seminar.register.action',['seminar_id' => $seminar_id,'message' => '既に定員に達しているため、申込できませんでした'])]);
        }else{
          $data = new SeminarRegistration;
          $data->m_seminar_group_id = $group_id;
          $data->m_seminar_id = $seminar_id;
          $data->user_id = $user->id;
          $data->registration_at = Carbon::now();
          if($data->save()){
            $qr_code = $user->id.','.$group_id.','.$seminar_id.','.$data->id;
            $data->qr_code = $qr_code;
            $data->save();
            $action = 'success';
            return response()->json(['status' => true, 'redirect' => route('seminar.register.action',['seminar_id' => $seminar_id,'message' => '下記セミナーへの申込を行いました'])]);
          }
        }
      }
    }

    public function cancel(Request $request){ //seminar cancelation
      $user = Auth::user();
      $seminar_registration_id = $request->get('seminar_registration_id');
      $update = SeminarRegistration::where('user_id',$user->id)->where('id',$seminar_registration_id)->update(['canceled_at' => Carbon::now()]);

      if($update){
        $seminar_id = $request->get('seminar_id');
        return response()->json(['status' => true, 'redirect' => route('seminar.register.action',['seminar_id' => $seminar_id,'message' => '下記セミナーへの申込をキャンセルしました'])]);
      }
    }

    function register_action(Request $request){
      $seminar = Seminar::find($request->seminar_id);
      $message = $request->message;
      return view('Seminar.register_action',compact('seminar','message'));
    }

    public function user_detail($id){ //seminar detail

      $user = Auth::user();
      $seminar_registration = SeminarRegistration::where('id',$id)->where('user_id',$user->id)->whereNull('canceled_at')->first();

      if($seminar_registration){
        
        $seminar = Seminar::find($seminar_registration->m_seminar_id);
        //check if the user data in user_seminar_status
        $user_seminar_status = UserSeminarStatus::where('seminar_registration_id',$seminar_registration->id);

        //user exam
        $seminar_id = $seminar_registration->m_seminar_id;
        $user_exams = UserExam::whereHas('exam_info', function ($query) use($seminar_id) {
                                          $query->where('m_seminar_id',$seminar_id);})->where('seminar_registration_id',$seminar_registration->id)->where('user_id',$user->id)->get();
        
        return view('Seminar.user_seminar_detail' ,compact('seminar','user_seminar_status','seminar_registration','user_exams'));
      }else{
        return redirect()->route('account.index');
      }

    }
}