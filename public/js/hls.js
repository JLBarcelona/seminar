class HlsClient {

    #video;
    #arrSkipInfo;
    #eventHMovieEndFunc;

    constructor(view, width, height, skipInfo, eventHMovieEndFunc) {

        this.#arrSkipInfo = skipInfo;
        this.#eventHMovieEndFunc = eventHMovieEndFunc;

        this.#video = videojs(view, {
            width: width,
            height: height,
            loop: false,
            controls: true,
            preload: 'auto',
            muted:"muted",
        });

        this.#video.on(['loadstart', 'loadedmetadata', 'loadeddata',
                'play', 'playing', 'pause', 'suspend', 'seeking', 'seeked',
                'waiting', 'canplay', 'canplaythrough', 'ratechange',
                'ended', 'emptied', 'error', 'abort', 'muted', 'fullscreenchange'], (e) => {
            //console.log(`EVENT: ${e.type}`); //uncomment this for testing/checking events
            if (e.type == 'ended') {
                this.#eventHMovieEndFunc();
            }

            if (e.type == 'fullscreenchange') {
                this.#video.play();
                this.#video.muted(false);
                //console.log('triggered');
            }

            if(e.type == 'playing'){
                this.#video.muted(false);
            }

        });
    }

    setSrc(src) {
        this.#video.src({
            type: 'application/x-mpegURL',
            src: src
        });
    }

    setMuted(muted){
        this.#video.muted(muted);
    }

    setVolume(){
        this.#video.volume(1);
    }

    play() {
        this.#video.play();
    }

    skip() {
        for (let idx = 0; idx < this.#arrSkipInfo.length; idx++) {
            if (this.#video.currentTime() < this.#arrSkipInfo[idx]) {
                this.#video.currentTime(this.#arrSkipInfo[idx]);
                break;
            }
        }
    }
}