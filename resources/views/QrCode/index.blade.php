@extends('Layout.qrcode.app')
@section('title', '今後の講座 予定')
@section('css')
@endsection

@php $user = Auth::user(); @endphp

@section('content')
	<div class="row justify-content-center" style="margin-top: 165px;">
		<div class="col-md-5 col-12">
            <div class="card">
				<div class="card-body text-primary">
					<div class="row">
						<div class="col-md-4 col-4">
							<i class="fa fa-user fa-3x"></i>
						</div>
						<div class="col-md-8 col-8 mt-2">
							<h3><b>{{ $user->l_name }} {{ $user->f_name }}</b></h3>
						</div>
						<div class="col-md-12 col-12 mt-5">
							<p class="m-1"><b>セミナー番号 {{ $seminar->seminar->category_no }} 領域 {{ $seminar->seminar->category_no }}</b></p>
							<p class="m-1"><b>{{ $seminar->seminar->category_name }} / {{ $seminar->seminar->seminar_title }}</b></p>

							<div class="text-center mt-3 mb-5">
								<center><div id="qrcode"></div></center>
								<h1>{{ $seminar->qr_code }}</h1>
							</div>

							<p class="m-1"><b>会場に設置されているQRコードリーダーに表示されているQRコードを読み込ませてください。</b></p>
							<p class="m-1"><b>読み込み処理後、１分経過しても表示画面が変わらない場合は、会場スタッフまでご連絡ください。</b></p>
						</div>
                        <div class="col-sm-12 text-center mt-3">
                            <a href="{{route('seminar.user.detail',$seminar->id)}}" class="btn btn-primary">戻る</a>
                         </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/qrcode.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var qrcode = new QRCode("qrcode", {
		    text: "{{ $seminar->qr_code }}",
		    width: 180,
		    height: 180,
		    colorDark : "#0004FD",
		    colorLight : "#ffffff",
		    correctLevel : QRCode.CorrectLevel.H
		});
	});

	function check_qr_reception(){
		$.ajax({
		    type:"GET",
		    url:"{{ route('qrcode.check.seminar', [$seminar->id]) }}",
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		      if (response.status) {
			      window.location = response.redirect;
		      }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

	setInterval(function(){
		check_qr_reception();
	}, 3000);
</script>
@endsection
