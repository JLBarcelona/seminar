@extends('Layout.qrcode.app')
@section('title', '今後の講座 予定')
@section('css')
@endsection

@php $user = Auth::user(); @endphp

@section('content')
	<div class="row justify-content-center" style="margin-top: 165px;">
		<div class="col-md-5 col-12">
            <div class="card">
				<div class="card-body text-primary">
					<div class="row">
						<div class="col-md-4 col-4">
							<i class="fa fa-user fa-3x"></i>
						</div>
						<div class="col-md-8 col-8 mt-2">
							<h3><b>{{ $user->l_name }} {{ $user->f_name }}</b></h3>
						</div>
						<div class="col-md-12 col-12 mt-5">
							<p class="m-1"><b>セミナー番号 {{ $seminar->seminar->category_no }}</b></p>
							<p class="m-1"><b>領域 {{ $seminar->seminar->category_name }} {{ $seminar->seminar->seminar_title }}</b></p>

							<div class="text-center mt-3 mb-5">
								<i class="fa fa-check text-primary fa-5x"></i>
							</div>
							<p class="m-1 text-center">退席確認が完了しました</p>
						</div>
						<div class="col-md-12 col-12 text-center">
							<a href="{{ route('account.index') }}" class="btn btn-outline-primary px-5">講義詳細へ戻る</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
