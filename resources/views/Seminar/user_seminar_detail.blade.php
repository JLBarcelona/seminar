@extends('Layout.account.app')
@section('title', 'セミナー詳細')

@section('css')
@endsection

@section('content')
<div class="row justify-content-center mt-5">
   <div class="col-md-5 col-12">
      <div class="row">
         <div class="col-sm-12 col-12">
            <div class="text-primary">
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-calendar-alt"> 日  程 </i>
                  </div>
                  <div class="col-8">
                     <b class="">{{ japan_date($seminar->seminar_date) }}</b>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-clock"> 開催時間</i>
                  </div>
                  <div class="col-8">
                     <b class="">{{ $seminar->start_at->format('g:i')." ~ ".$seminar->end_at->format('g:i') }}</b>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-pencil-alt"> 受講枠</i>
                  </div>
                  <div class="col-8">
                     <p class="mb-0">セミナー番号{{ $seminar->category_no }} 総論{{ $seminar->category_no }} ({{ $seminar->category_name }})</p>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="text-primary fa fa-mobile-alt"> 受付時間</i>
                  </div>
                  <div class="col-8">
                     <p class="mb-0">{{ japan_date($seminar->seminar_date,'md') }} {{ $seminar->reception_open_at->format('g:i')." ~ ".$seminar->reception_close_at->format('g:i') }}</p>
                  </div>
               </div>

               @if($user_seminar_status->count() == 0 || (!empty($user_seminar_status->first()->exam_answered_at) && empty($user_seminar_status->first()->reception_out_at)) )
                  <div class="row p-4">
                     <div class="col text-center">
                       <b class="text-danger"> QRコードで参加受付を行ってください</b>
                     </div>
                  </div>
               @endif

               @if($seminar->reception_open_at > now())
                  <div class="row mb-3">
                     <div class="col text-center">
                       <b class="text-danger"> 受付開始日時までお待ちください</b>
                     </div>
                  </div>
               @else
                  <div class="row mb-3">
                     <div class="col text-center">
                        @if($user_seminar_status->count() == 0)
                           <button type="button" class="text-sm btn btn-primary btn-block">参加受付</button>
                        @else
                           <button type="button" class="text-sm btn btn-light btn-block text-primary">参加受付</button>
                        @endif
                     </div>
                     <div class="col text-center">
                        @if(!empty($user_seminar_status->first()->reception_in_at) && empty($user_seminar_status->first()->exam_answered_at))
                           <button type="button" class="text-sm btn btn-primary border-0 btn-block">テスト回答</button>
                        @else
                           <button type="button" class="text-sm btn btn-light border-0 btn-block text-primary">テスト回答</button>
                        @endif
                     </div>
                     <div class="col text-center">
                        @if(!empty($user_seminar_status->first()->exam_answered_at) && empty($user_seminar_status->first()->reception_out_at))
                           <button type="button" class="text-sm btn btn-primary border-0 btn-block">退席確認</button>
                        @else
                           <button type="button" class="text-sm btn btn-light border-0 btn-block text-primary">退席確認</button>
                        @endif
                     </div>
                  </div>

                  <div class="row mb-3">
                     <div class="col text-center">
                     @if($user_seminar_status->count() == 0)
                        <i class="fas fa-exclamation-circle fa-3x text-info"></i>
                     @else
                        <i class="fas fa-check-circle fa-3x text-secondary"></i>
                     @endif
                     </div>
                     <div class="col text-center">
                       <button class="btn btn-sm text-info"><i class="fas fa-arrow-right fa-3x"></i></button>
                     </div>
                     <div class="col text-center">
                        @if(!empty($user_seminar_status->first()->reception_in_at) && empty($user_seminar_status->first()->exam_answered_at))
                           <i class="fas fa-exclamation-circle fa-3x text-info"></i>
                        @elseif(!empty($user_seminar_status->first()->reception_in_at) && !empty($user_seminar_status->first()->exam_answered_at))
                           <i class="fas fa-check-circle fa-3x text-secondary"></i>
                        @else
                           <i class="fas fa-times-circle fa-3x text-secondary"></i>
                        @endif
                     </div>
                     <div class="col text-center">
                       <button class="btn btn-sm text-info"><i class="fas fa-arrow-right fa-3x"></i></button>
                     </div>
                     <div class="col text-center">
                        @if(!empty($user_seminar_status->first()->exam_answered_at) && empty($user_seminar_status->first()->reception_out_at))
                           <i class="fas fa-exclamation-circle fa-3x text-info"></i>
                        @elseif(!empty($user_seminar_status->first()->exam_answered_at) && !empty($user_seminar_status->first()->reception_out_at))
                           <i class="fas fa-check-circle fa-3x text-secondary"></i>
                        @else
                           <i class="fas fa-times-circle fa-3x text-secondary"></i>
                        @endif
                     </div>
                  </div>
                  
                  @if($user_seminar_status->count() == 0)

                  <div class="row mb-3">
                     <div class="col text-center">
                       <a href="{{ route('qrcode.index',[$seminar_registration->id,$seminar->id]) }}" class="btn btn-primary w-50">QRコード</a>
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col text-center">
                       QRコードで参加受付を行ってください
                     </div>
                  </div>

                  <div class="row mb-3">
                     <div class="col text-center">
                       <button disabled="" class="btn btn-secondary w-50"> テスト回答 </button>
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col text-center text-secondary">
                       まだ受付登録が完了していません
                     </div>
                  </div>
               @elseif(!empty($user_seminar_status->first()->reception_in_at) && empty($user_seminar_status->first()->exam_answered_at))

                  <div class="row mb-3">
                     <div class="col text-center">
                       <a href="{{ route('seminar_exam.index',$seminar_registration->id)}}" class="btn btn-primary w-50"> テスト回答 </a>
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col text-center text-secondary">
                       テスト回答ボタンよりテストを受けてください
                     </div>
                  </div>

               @elseif(!empty($user_seminar_status->first()->exam_answered_at) && empty($user_seminar_status->first()->reception_out_at))

                  <div class="row mb-3">
                     <div class="col text-center">
                       <a href="{{ route('qrcode.index',[$seminar_registration->id,$seminar->id]) }}" class="btn btn-primary w-50">QRコード</a>
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col text-center">
                       QRコードで退席確認を行ってください
                     </div>
                  </div>
               @else
               <div class="row mb-3"></div>
               @endif

               <div class="row mb-3">
                  <div class="col-3 text-center">
                     <hr>
                  </div>
                  <div class="col-6 text-center text-secondary">
                    ご登録の回答内容
                  </div>
                  <div class="col-3 text-center">
                    <hr>
                  </div>
               </div>

               <!-- Questions here -->
               <div class="row text-center">
                  @forelse ($user_exams as $user_exam)
                     <div class="col">
                        <p class="text-center">問題 {{ $user_exam->exam_info->question_no }}</p>
                        <button type="button" class=" btn btn-block {{ ($user_exam->answer == 1) ? 'btn-primary' : 'btn-light text-primary' }}" >はい</button>
                        <button type="button" class=" btn btn-block {{ ($user_exam->answer == 0) ? 'btn-primary' : 'btn-light text-primary' }}">いいえ</button>
                     </div>
                  @empty
                     @for ($i = 0; $i < 3; $i++)
                        <div class="col-4">
                           <p class="text-center">問題 {{ $i + 1 }}</p>
                           <button type="button" class=" btn btn-block btn-secondary">&nbsp;</button>
                           <button type="button" class=" btn btn-block btn-secondary">&nbsp;</button>
                        </div>
                     @endfor
                  @endforelse
               </div>

               @endif

            </div>
         </div>

         <div class="col-sm-12 text-center mt-3">
            <a href="{{route('seminar.index',$seminar->m_seminar_group_id)}}" class="btn btn-primary">戻る</a>
         </div>

      </div>
   </div>
</div>

@endsection

@section('script')
@endsection
