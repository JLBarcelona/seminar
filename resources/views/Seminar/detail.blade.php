@extends('Layout.account.app')
@section('title', '今後の講座 予定')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
<div class="row justify-content-center mt-5">
   <div class="col-md-5 col-12">
      <div class="row">
         <div class="col-sm-12 col-12">
            <div class="text-primary">
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-calendar-alt"> 日  程 </i>
                  </div>
                  <div class="col-8">
                     <b class="">{{ japan_date($seminar->seminar_date) }}</b>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-clock"> 開催時間</i>
                  </div>
                  <div class="col-8">
                     <b class="">{{ ltrim($seminar->start_at->format('H:i'),"0")." ~ ".ltrim($seminar->end_at->format('H:i'),"0") }}</b>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="fa fa-pencil-alt"> 受講枠</i>
                  </div>
                  <div class="col-8">
                     <p class="mb-0">セミナー番号{{ $seminar->category_no }} 領域{{ $seminar->category_no }} <br>({{ $seminar->category_name }}) / {{ $seminar->seminar_title }}</p>
                  </div>
               </div>
               <div class="row mb-2">
                  <div class="col-4">
                     <i class="text-primary fa fa-mobile-alt"> 受付時間</i>
                  </div>
                  <div class="col-8">
                     <p class="mb-0">{{ japan_date($seminar->seminar_date,'md') }} {{ ltrim($seminar->reception_open_at->format('H:i'),"0")." ~ ".ltrim($seminar->reception_close_at->format('H:i'),"0") }}</p>
                  </div>
               </div>
               @if($capacity_left >= 0.9)
	               <div class="row p-4">
	                  <div class="col text-center">
	                     <b class="text-danger"> 既に定員に達しています</b>
	                  </div>
	               </div>
               @endif
            </div>
            <input type="hidden" id="group_id" name="group_id" value="{{ $seminar->m_seminar_group_id }}">
            <input type="hidden" id="seminar_id" name="seminar_id" value="{{ $seminar->id }}">
            <input type="hidden" id="seminar_registration_id" name="seminar_registration_id" value="{{ ($with_registration->count() > 0) ? $with_registration->first()->id : '' }}">
            <div class="row">
               <div class="col">
                  <a type="button" class="btn btn-light border-primary btn-block text-primary"  href="{{ route('seminar.index',$seminar->m_seminar_group_id) }}" style="font-size: 14px">セミナー一覧へ戻る</a>
               </div>
               <div class="col">
                  @if($with_registration->count() > 0)
                  	<button type="button" class="text-sm btn btn-light border-primary btn-block text-danger" data-url="{{ route('seminar.cancel') }}" id="btn_cancel">申込キャンセル</button>
                  @else
                     @if(capacity_left($seminar->id) >= 1)
                        <button type="button" class="text-sm btn btn-light border-primary btn-block text-primary" disabled="">参加申込</button>
                     @else
                        <button type="button" class="text-sm btn btn-light border-primary btn-block text-primary" data-url="{{ route('seminar.register') }}" id="btn_register">参加申込</button>
                     @endif
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/seminar_registration.js')}}"></script>
@endsection
