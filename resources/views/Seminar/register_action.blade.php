@extends('Layout.account.app')
@section('title', '今後の講座 予定')
@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
            <div class="row">
				<div class="col-sm-12 text-center">
					<label class="page-title">
						{{ $message }}
					</label>
				</div>
				<div class="col-sm-12 col-12 text-center">
					<p class="fs-4 fw-bold"><strong>セミナー番号 {{ $seminar->category_no }} 領域 {{ $seminar->category_no }} <br> {{ $seminar->category_name }}</strong></p>
				</div>
         		<div class="col-sm-12 text-right"> 
         			{{-- <a type="button" class="btn btn-light border-primary text-primary float-end"  href="{{ route('seminar.show',$seminar->m_seminar_group_id) }}">戻る</a> --}}
         			<a type="button" class="btn btn-light border-primary text-primary float-end"  href="{{ URL::previous() }}">戻る</a>
         		</div>
			</div>
		</div>
	</div>
@endsection
