@extends('Layout.account.app')
@section('title', '今後の講座 予定')

@section('css')
<style type="text/css">

</style>
@endsection


@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">

                <div class="row">

					<div class="col-sm-12 text-center">
						<label class="page-title">今後の講座 予定</label>
					</div>

					@if($seminar_group)

						@if(!$seminar_group->seminars->isEmpty())

							@foreach($seminar_group->seminars as $seminar)

							@php $capacity = capacity_left($seminar->id)  @endphp


								<div class="col-md-12 col-12">

									<div class="card seminar-card border-primary">

									  <div class="card-body text-primary">

									    <div class="row mb-2">

								    		<div class="col-4">
										      <i class="fa fa-calendar-alt"> 日  程 </i>
										    </div>
										    <div class="col-8">
										     	<b class="">{{ japan_date($seminar->seminar_date) }}</b>
										    </div>

									  	</div>

									  	<div class="row mb-2">
									  		<div class="col-4">
										      <i class="fa fa-clock"> 開催時間</i>
										    </div>
										    <div class="col-8">
										     	<b class="">{{ ltrim($seminar->start_at->format('H:i'),"0")." ~ ".ltrim($seminar->end_at->format('H:i'),"0") }}</b>
										    </div>
										</div>

										<div class="row mb-2">
									  		<div class="col-4">
										      <i class="fa fa-pencil-alt"> 受講枠</i>
										    </div>
										    <div class="col-8">
										     	<p class="mb-0">セミナー番号 {{ $seminar->category_no }} 領域 {{ $seminar->category_no }}</p>
										     	<p class="mb-0">{{ $seminar->category_name }} / {{ $seminar->seminar_title }}</p>
										    </div>
										</div>

										<div class="row mb-2">
									  		<div class="col-4">
										      <i class="text-primary fa fa-mobile-alt"> 受付時間</i>
										    </div>
										    <div class="col-8">
										     	<p class="mb-0">{{ japan_date($seminar->seminar_date,'md') }} {{ ltrim($seminar->reception_open_at->format('H:i'),"0")." ~ ".ltrim($seminar->reception_close_at->format('H:i'),"0") }}</p>
										    </div>
										</div>
										@if($capacity >= 0.9 && $capacity < 1)
											<div class="row">
										  		<div class="col text-center">
											      <b class="text-danger"> 定員まで残り僅かです</b>
											    </div>
											</div>
										@elseif($capacity >= 1)
											<div class="row">
										  		<div class="col text-center">
											      <b class="text-danger"> 既に定員に達しています</b>
											    </div>
											</div>
										@endif

									  </div>

									  <div class="card-body">

									  	<div class="row">

											<!-- seminar registration -->
											@if($seminar_group->registration_from <= $today_date && $seminar_group->registration_to >= $today_date)
												@php

													$user_seminar = App\Models\SeminarRegistration::
													where('user_id', Auth::user()->id)
													->where('m_seminar_group_id',$seminar_group->id)
													->where('m_seminar_id',$seminar->id)
													->whereNull('canceled_at')
													->first();

												@endphp

												@if(!$user_seminar)
												  	<div class="col">
												  		@if($capacity >= 1)
												  			<button class="btn_register btn btn-primary btn-block" disabled=""> 参加申込 </button>
												  		@else
												  			<a data-url="{{ route('seminar.register') }}" data-seminar_id="{{ $seminar->id }}" data-group_id="{{ $seminar_group->id }}" class="btn_register btn btn-primary btn-block">参加申込</a>
												  		@endif
												    </div>
												@endif

											@endif

											<!-- redirect seminar detail (8) -->
											@if(Carbon\Carbon::parse($seminar->reception_open_at)->subHours(1) <= $today_date && Carbon\Carbon::parse($seminar->end_at)->addHours(1) >= $today_date)
												@php 
												
													$user_seminar = App\Models\SeminarRegistration::
													where('user_id', Auth::user()->id)
													->where('m_seminar_group_id',$seminar_group->id)
													->where('m_seminar_id',$seminar->id)
													->whereNull('canceled_at')
													->first();

												@endphp

												@if($user_seminar)
												  	<div class="col">
												      <a href="{{route('seminar.user.detail',$user_seminar->id)}}" class="btn btn-primary btn-block">QRコード　詳細</a>
												    </div>
												@endif

											@endif

											<!-- cancel seminar registration -->
											@if($seminar_group->registration_from <= $today_date && $seminar_group->registration_to >= $today_date)
												@php

													$user_seminar = App\Models\SeminarRegistration::
													where('user_id', Auth::user()->id)
													->where('m_seminar_group_id',$seminar_group->id)
													->where('m_seminar_id',$seminar->id)
													->whereNull('canceled_at')
													->first();

												@endphp

												@if($user_seminar)
												  	<div class="col">
												      <a data-url="{{ route('seminar.cancel') }}" data-id="{{ $user_seminar->id }}" data-seminar_id="{{ $user_seminar->m_seminar_id }}" class="btn btn_cancel btn-light border-primary btn-block text-danger">申込キャンセル</a>
												    </div>
												@endif

											@endif


									 	</div>

									  </div>

									</div>

								</div>

							@endforeach

						@endif

					@else

						<!-- error message -->

					@endif


				</div>

             	<div class="col-sm-12 text-center">
					<a href="{{route('account.index')}}" class="btn btn-primary">HOMEへ戻る</a>
				</div>


		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('js/seminar_index.js')}}"></script>
@endsection
