@extends('Layout.app')
@section('title', 'パスワード再設定完了')

@section('css')
<style type="text/css">
	
</style>
@endsection

@section('content')
	<div class="col-md-5 col-12">
        <label class="page-title">パスワード再設定完了</label>
    </div>

    <div class="col-sm-12 col-12 text-center">
        <p class="fs-4 fw-bold"><strong>パスワード変更しました。</strong></p>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col-sm-3 text-center">
            <a href="{{ route('login') }}" class="btn btn-outline-primary w-100">ログイン画面に戻る</a>
        </div>
    </div>

@endsection

@section('script')
<script type="text/javascript"></script>
@endsection