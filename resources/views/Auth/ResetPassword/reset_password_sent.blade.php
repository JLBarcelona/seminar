@extends('Layout.app')
@section('title', 'パスワード再設定')

@section('css')
<style type="text/css">
	
</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<!-- input start -->
			<div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">パスワードリセット</label>
				</div>
				<!-- form label end -->
				<div class="col-sm-12 col-12 text-center">
					<p class="fs-4 fw-bold"><strong>ご登録いただいたメールアドレス宛に、{{ get_management_company_detail()->support_mail_address }} からパスワードリセット画面のURLを送信しました。</strong></p>
					<p class="fs-4 fw-bold"><strong>URLへアクセスしていただき、パスワードリセットを行ってください。</strong></p>
					<div class="row justify-content-center">
						<div class="col-sm-6 text-center">
							<form class="needs-validation" id="resend_password_form" action="{{ route('auth.forget.password.post') }}" novalidate>
								<input type="hidden" id="mail_address" name="mail_address" value="{{ !empty(Request::get('email')) ? Request::get('email') : '' }}">
								<input type="hidden" name="resent" value="1">
								<button  class="btn btn-link fw-bold" type="submit" id="submit_button"><strong>メールを再送する</strong></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	$("#resend_password_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		        if(response.status){
		        }else{
		            showValidator(response.error,'forget_password_form');
		        }
		        $("#submit_button").prop('disabled', false);
		    },
		    error:function(error){
				$("#submit_button").prop('disabled', false);
		    }
		});
	});
</script>
@endsection

