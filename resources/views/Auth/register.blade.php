@extends('Layout.app')
@section('title', '受溝者情報登録')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="registration" action="{{ route('registration.store') }}" method="POST" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">受溝者情報登録</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">氏</span>
						  </div>
						  <input type="text" class="form-control" placeholder="田中" name="l_name" id="l_name" aria-describedby="basic-addon1">
						   <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_l_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">名</span>
						  </div>
						  <input type="text" class="form-control" name="f_name" placeholder="太郎" id="f_name" aria-describedby="basic-addon2">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_f_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon3">氏（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control" placeholder="タナカ" name="l_name_kana" id="l_name_kana" aria-describedby="basic-addon3">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
                          <div class="invalid-feedback" id="err_l_name_kana" data-custom-validator="カタカナで入力してください。"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon4">名（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control" placeholder="タロウ" name="f_name_kana" id="f_name_kana" aria-describedby="basic-addon4">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
                          <div class="invalid-feedback" id="err_f_name_kana" data-custom-validator="カタカナで入力してください。"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon5">会員番号</span>
						  </div>

						  <input type="text" class="form-control col-2 text-center" placeholder="ア" name="certificate_id1" maxlength="1" id="certificate_id1">
				    	 

				    	  <div class="col-1 text-center">
				    	  	<h3 class="mb-0">-</h3>
				    	  </div>

			    	   <input type="number" class="form-control col-2" name="certificate_id2" min="1" maxlength="4" placeholder="1234" id="certificate_id2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">

			    	   <div class="col border-0 text-right bg-white m-0" style="padding: 10px 10px 0px 0px;" id="basic-addon1">
					    	<i class="fa fa-asterisk fa-1x text-danger"></i>
					   </div>

			    	    <div class="invalid-feedback" id="err_certificate_id1" data-custom-validator="カタカナで入力してください。"></div>
						<div class="invalid-feedback" id="err_certificate_id2" data-custom-validator="9999 までの数字で入力してください"></div>

						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon6">参加証番号</span>
						  </div>
						  <input type="text" class="form-control" placeholder="12345" name="registration_id" id="registration_id" aria-describedby="basic-addon7">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-white"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_registration_id"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon7">Emailアドレス</span>
						  </div>
						  <input type="email" class="form-control" placeholder="aaaa@bbbb.com" name="mail_address" id="mail_address" aria-describedby="basic-addon8">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
                          <div class="invalid-feedback" id="err_mail_address" data-custom-validator="入力されたメールアドレスは既に登録されています"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon8">所属施設</span>
						  </div>
						  <input type="text" class="form-control" placeholder="あいうえ病院" name="hospital_name" id="hospital_name" aria-describedby="basic-addon9">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_hospital_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon9">所属部署</span>
						  </div>
						  <input type="text" class="form-control" placeholder="内科" name="devision_name" id="devision_name" aria-describedby="basic-addon10">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_devision_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon11">TEL</span>
						  </div>
                            <input type="text" pattern = "[0-9]+([-\,][0-9]+)?"  maxlength="16" onkeypress="return onlyNumberKeyandDash(event, this.value)"  placeholder="03-1111-1111" name="tel" id="tel" class="form-control">
                            <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
					        <div class="invalid-feedback" id="err_tel"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon11">生年月日</span>
						  </div>
						  <input type="date" class="form-control" value="1970-01-01" name="birthday" id="birthday" aria-describedby="basic-addon12">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>

				    	  <div class="invalid-feedback" id="err_birthday"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon12">パスワード</span>
						  </div>
						  <input type="password" class="form-control" placeholder="英数字8桁（半角英字、数字）" min="8" max="8" maxlength="8" name="password" id="password" aria-describedby="basic-addon13">
						  <div class="input-group-append bg-primary input-label">
						    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
						  </div>
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_password"></div>
						</div>
					</div>

				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end">
					<div class="col-md-8 text-right">
						<button type="submit" class="btn btn-outline-primary" id="submit_button">受講者情報を登録する</button>
					</div>
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	$("#registration").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		        $("#submit_button").prop('disabled', false);
		        if(response.status == true){
		           showValidator(response.error,'registration');
		           window.location = response.redirect;
		        }else{
		            showValidator(response.error,'registration');
		        }
		    },
		    error:function(error){
		        console.log(error);
		        $("#submit_button").prop('disabled', false);
		    }
		});
	});

    function onlyNumberKeyandDash(evt, tel) {
		  var ASCIICode = (evt.which) ? evt.which : evt.keyCode
		  let last_char = tel.charAt(tel.length - 1);
		if((tel === '' && ASCIICode === 45) || (tel !== '' && ASCIICode === 45 && last_char === '-')){
			return false;
		}else{
			if(ASCIICode === 45){
				return true;
			}
			if ((ASCIICode < 48 || ASCIICode > 57))
				return false;
			return true;
		}
	  }

</script>
@endsection
