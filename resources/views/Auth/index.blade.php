@extends('Layout.app')
@section('title', 'Login')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="login_form" action="{{ route('auth.authenticate') }}" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">ログイン</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">Emailアドレス</span>
						  </div>
						  <input type="email" class="form-control" name="mail_address" id="mail_address" aria-describedby="basic-addon1" autocomplete="false">
				    	  <div class="invalid-feedback" id="err_mail_address"></div>
						</div>
					</div>
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">パスワード</span>
						  </div>
						  <input type="password" class="form-control" name="password" maxlength="8" id="password" aria-describedby="basic-addon2" autocomplete="off">
						   <div class="input-group-append bg-primary input-label">
						    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
						  </div>
						  <div class="invalid-feedback" id="err_password"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				@if(!empty(Request::get('authenticate')))
					<div class="row" id="lbl_error">
						<div class="col-12 text-danger">
							メール認証が完了しておりません。 <br>
							メール内に記載のURLよりメール認証を完了させてください。 <br>
							もし、メールが届いていない場合は、お手数ですが+Seminarサポートまでご連絡ください。 <br>
							+Seminarサポート ： jba-support@plus-seminar.com
						</div>
					</div>
				@endif
				<!-- button start -->
				<div class="row justify-content-end mb-2">
					<div class="col-sm-12 text-right mb-2">
						<div class="row justify-content-end">
							<div class="col-5"></div>
							<div class="col-7 col-md-4"><button type="submit" class="btn btn-outline-primary px-5 col-12" id="submit_button">ログイン</button></div>
						</div>						
					</div>
					<div class="col-sm-12 text-right mb-2">
						<div class="row justify-content-end">
							<div class="col-5"><small style="font-size: 10px;"><b>パスワードを忘れた方はこちら</b></small></div>
							<div class="col-7 col-md-4"><a href="{{ route('auth.forget.password.get') }}" class="btn btn-outline-primary px-5 col-12" id="submit_button">パスワード再設定</a></div>
						</div>						
					</div>
					<div class="col-sm-12 text-right mb-2">
						<div class="row justify-content-end">
							<div class="col-5"><small style="font-size: 10px;"><b>ユーザー登録がまだの方はこちら</b></small></div>
							<div class="col-7 col-md-4"><a href="{{ route('registration') }}" class="btn btn-outline-primary px-5 col-12 " id="submit_button">ユーザー登録</a></div>
						</div>						
					</div>
				</div>
				<!-- button end -->
				<!-- message start -->
				<div class="row justify-content-center mb-2">
					<div class="col-sm-12 text-right mb-2">
						<div class="row justify-content-center">
							<div class="col-2 text-center"></div>
							<div class="col-8">
								<p style="font-size: 10px;" class="text-left text-danger">
									指導医養成講座を新規に申込される方は、「ユーザー登録がまだの方は
									こちら」より、アカウントの新規登録を行ってください。<br>
									第５８回学術会の参加登録とは別にご登録いただく必要があります。
								</p>
							</div>
							<div class="col-2 text-center"></div>
						</div>						
					</div>
				</div>
				<!-- message end -->
			</form>
		</div>
	</div>
@endsection


@section('script')
<script type="text/javascript" src="{{asset('js/ajaxsetup.js')}}"></script>

<script type="text/javascript">
	$("#login_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		        $("#submit_button").prop('disabled', false);
		        if(response.status == true){
		          showValidator(response.error,'login_form');
		          window.location = response.redirect;
		        }else{
		        	@if(!empty(Request::get('authenticate')))
		        		$('#lbl_error').addClass('d-none');
		        	@endif
		            showValidator(response.error,'login_form');
		        }
		    },
		    error:function(error){
		        console.log(error);
		        $("#submit_button").prop('disabled', false);
		    }
		});
	});


</script>

@endsection
