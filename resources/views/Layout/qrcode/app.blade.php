<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
		<style type="text/css">
			.img-bg{
				background-attachment: fixed;
				background-repeat: no-repeat;
				background-size: cover;
				background-position: center;
			}
		</style>
	</head>
	<body class="hold-transition img-bg" style="background-image: url('{{ asset('img/bg.jpg')  }}'); overflow-x: hidden;">
		<div class="">
			<div class="">
			 	 <section class="content">
					 <div class="container-fluid">
						@yield('content')
					</div>
				 </section>
			</div>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>