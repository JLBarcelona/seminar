
<nav class="main-header main-header-seminar navbar navbar-expand navbar-lightgray navbar-light row justify-content-center">

    <div class="header-text">
      <h3 class="text-primary mb-0 font-weight-bold">+Seminar 開発環境</h3>
    </div>
    
</nav>


<div class="brand-logo row justify-content-center">

  <img src="{{ asset('img/logo.png') }}" alt="Seminar Logo" class="brand-image" style="">

</div>

<hr class="nav-hr-border">

{{-- 
<div class="bg-light shadow-sm pt-5 pb-2 row justify-content-center">
 
</div>

<div class="bg-white row justify-content-center pt-2" style="border-bottom: solid 3px #555; ">
  <div class="col-sm-4 text-center">
      <img src="{{ asset('img/logo.png') }}" class="img-fluid" style="margin-right: 98px;" width="320">
  </div>
</div> --}}

{{-- 
<div class="user-panel row justify-content-center">

    <div class="info text-center">
      <p class="text-primary">日本 胆道 学会</p>
      <p class="text-primary">指導医養成 講座</p>
    </div>
</div> --}}
