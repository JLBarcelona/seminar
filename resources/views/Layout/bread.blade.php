@php $i = 0; $total = count($data); @endphp
@php $link = route('account.index') @endphp
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"><b>{{ $title }}</b></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ $link }}">Home</a></li>
            @foreach($data as $key => $val)
              @php $i++;  @endphp
                @if($i == $total && $total != 1)
                   <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
                @elseif($total == 1)
                  <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
                @else
                    <li class="breadcrumb-item"><a href="{{ $val }}">{{ $key }}</a></li>
                @endif
            @endforeach
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>