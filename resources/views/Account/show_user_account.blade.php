@extends('Layout.account.app')
@section('title', '受講者情報')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="" id="registration" action="#" method="POST" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">受講者情報</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">氏</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="l_name" id="l_name" aria-describedby="basic-addon1" value="{{$user->l_name}}" readonly>
				    	  <div class="invalid-feedback" id="err_l_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">名</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="f_name" id="f_name" aria-describedby="basic-addon2" value="{{$user->f_name}}" readonly>
				    	  <div class="invalid-feedback" id="err_f_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon3">氏（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="l_name_kana" id="l_name_kana" aria-describedby="basic-addon3" value="{{$user->l_name_kana}}" readonly>
				    	  <div class="invalid-feedback" id="err_l_name_kana"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon4">名（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="f_name_kana" id="f_name_kana" aria-describedby="basic-addon4" value="{{$user->f_name_kana}}" readonly>
				    	  <div class="invalid-feedback" id="err_f_name_kana"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon5">会員番号</span>
						  </div>

						  <input type="text" class="form-control col-2 text-center bg-white" name="certificate_id1" maxlength="1" id="certificate_id1" value="{{$user->certificate_id1}}" readonly>
				    	 

				    	  <div class="col-1 text-center">
				    	  	<h3 class="mb-0">-</h3>
				    	  </div>

			    	   <input type="number" class="form-control col-2 bg-white" name="certificate_id2" min="1" maxlength="4" id="certificate_id2" value="{{$user->certificate_id2}}" readonly>

						</div>
					</div>



					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon6">参加証番号</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="registration_id" id="registration_id" aria-describedby="basic-addon7" value="{{$user->registration_id}}" readonly>
				    	  <div class="invalid-feedback" id="err_registration_id"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon7">Emailアドレス</span>
						  </div>
						  <input type="email" class="form-control bg-white" name="mail_address" id="mail_address" aria-describedby="basic-addon8" value="{{$user->mail_address}}" readonly>
				    	  <div class="invalid-feedback" id="err_mail_address"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon8">所属施設</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="hospital_name" id="hospital_name" aria-describedby="basic-addon9" value="{{$user->hospital_name}}" readonly>
				    	  <div class="invalid-feedback" id="err_hospital_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon9">所属部署</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="devision_name" id="devision_name" aria-describedby="basic-addon10" value="{{$user->devision_name}}" readonly>
				    	  <div class="invalid-feedback" id="err_devision_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon11">TEL</span>
						  </div>
                            <input type="tel"  maxlength="16" placeholder="" name="tel" id="tel" class="form-control bg-white" value="{{$user->tel}}" readonly>
					        <div class="invalid-feedback" id="err_tel" data-custom-validator="数字とハイフンで入力してください"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon12">生年月日</span>
						  </div>
						  <input type="date" class="form-control bg-white" name="birthday" id="birthday" aria-describedby="basic-addon11" value="{{ !empty($user->birthday) ? $user->birthday : ''}}" readonly>
				    	  <div class="invalid-feedback" id="err_birthday"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end mb-2 text-center">
					<div class="col-md-6 col-5">
						<a href="{{ route('account.index') }}" class="btn btn-outline-primary">HOMEへ戻る</a>
					</div>
					<div class="col-md-6 col-7 ">
						<a href="{{ route('account.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary">受講者情報を変更する</a>
					</div>
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>
@endsection

