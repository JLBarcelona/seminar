@extends('Layout.account.app')
@section('title', '参加証アップロード')

@section('css')
<style type="text/css">

</style>
@endsection


@section('content')
	
	<form class="needs-validation" id="form_upload" action="{{ route('account.upload.certificate') }}" enctype="multipart/form-data" novalidate="">
    <div class="row justify-content-center mt-5">
      <div class="col-md-10 col-12">
          <div class="row justify-content-center">
          <!-- form label start -->
          <div class="col-sm-12">
            <label class="page-title">参加証アップロード</label>
          </div>

          <div class="col-sm-12">
            <div class="input-group mb-3">
              <input type="file" name="upload_file" id="upload_file" class="d-none" required="">
              <input type="type" class="form-control bg-white col-md-12" aria-describedby="basic-addon1" name="file_name" id="file_name"  readonly="">
                <div class="input-group-append bg-primary input-label">
                <button type="button" onclick="$('#upload_file').click();" class="input-group-text bg-primary border-0" id="basic-addon1">ファイル選択</button>
              </div>
                <div class="invalid-feedback" id="err_upload_file"></div>
            </div>
          </div>

          <div class="col-sm-12 text-right">
          	<a class="btn btn-warning" href="{{ route('account.index') }}" >戻る</a>
            <button class="btn btn-primary" id="submit_btn" type="submit"> アップロード </button>
          </div>

        </div>
      </div>
    </div>
</form>
@endsection

@section('script')
<script type="text/javascript">
  $("#form_upload").on('submit', function(e){
    e.preventDefault();
    let form = $('#form_upload')[0];
    let data = new FormData(form);
    let url = $('#form_upload').attr('action');

    $.ajax({
        type:"POST",
        url:url,
        processData: false,
        contentType: false,
        cache: false,
        data:data,
        dataType:'json',
        beforeSend:function(){
          $("#submit_btn").prop('disabled', true);
        },
        success:function(response){
           // console.log(response);
          if (response.status === true) {
            window.location = response.redirect;
          }else{
            showValidator(response.error, 'form_upload');
          }
          $("#submit_btn").prop('disabled', false);
        },
        error: function(error){
          $("#submit_btn").prop('disabled', false);
          console.log(error);
        }
      });
  });

  $("input[type=file]").on('change',function(){
    $("#file_name").val(this.files[0].name);
  });

</script>
@endsection
