@extends('Layout.account.app')
@section('title', '受講者情報変更')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="edit_user_account" action="{{ route('account.update', ['id' => $user->id]) }}" method="POST" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">受講者情報変更</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">氏</span>
						  </div>
						  <input type="text" class="form-control" placeholder="田中" name="l_name" id="l_name" aria-describedby="basic-addon1" value="{{$user->l_name}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_l_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">名</span>
						  </div>
						  <input type="text" class="form-control" placeholder="太郎" name="f_name" id="f_name" aria-describedby="basic-addon2" value="{{$user->f_name}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_f_name"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon3">氏（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control" placeholder="タナカ" name="l_name_kana" id="l_name_kana" aria-describedby="basic-addon3" value="{{$user->l_name_kana}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
                          <div class="invalid-feedback" id="err_l_name_kana" data-custom-validator="カタカナで入力してください。"></div>
                        </div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon4">名（ヨミガナ）</span>
						  </div>
						  <input type="text" class="form-control" placeholder="タロウ" name="f_name_kana" id="f_name_kana" aria-describedby="basic-addon4" value="{{$user->f_name_kana}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
                          <div class="invalid-feedback" id="err_f_name_kana" data-custom-validator="カタカナで入力してください。"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon5">会員番号</span>
						  </div>
						  <input type="text" class="form-control col-2 text-center" placeholder="ア" name="certificate_id1" maxlength="1" id="certificate_id1" value="{{$user->certificate_id1}}">
				    	  <div class="col-1 text-center">
				    	  	<h3 class="mb-0">-</h3>
				    	  </div>

			    	   <input type="number" class="form-control col-2" name="certificate_id2" min="1" maxlength="4" id="certificate_id2" value="{{$user->certificate_id2}}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="1234">

			    	   <div class="col border-0 text-right bg-white m-0" style="padding: 10px 10px 0px 0px;" id="basic-addon1">
					    	<i class="fa fa-asterisk fa-1x text-danger"></i>
					   </div>

			    	    <div class="invalid-feedback" id="err_certificate_id1" data-custom-validator="カタカナで入力してください。"></div>
						<div class="invalid-feedback" id="err_certificate_id2" data-custom-validator="9999 までの数字で入力してください"></div>

						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon7">参加証番号</span>
						  </div>
						  <input type="text" class="form-control" placeholder="12345" name="registration_id" id="registration_id" aria-describedby="basic-addon7" value="{{$user->registration_id}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-white"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_registration_id"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon8">Emailアドレス</span>
						  </div>
						  <input type="email" class="form-control" placeholder="aaaa@bbbb.com" name="mail_address" id="mail_address" aria-describedby="basic-addon8" value="{{$user->mail_address}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_mail_address"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon9">所属施設</span>
						  </div>
						  <input type="text" class="form-control" placeholder="あいうえ病院" name="hospital_name" id="hospital_name" aria-describedby="basic-addon9" value="{{$user->hospital_name}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_hospital_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon10">所属部署</span>
						  </div>
						  <input type="text" class="form-control" placeholder="内科" name="devision_name" id="devision_name" aria-describedby="basic-addon10" value="{{$user->devision_name}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
				    	  <div class="invalid-feedback" id="err_devision_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon11">TEL</span>
						  </div>
                            <input type="tel"  maxlength="16"  onkeypress="return onlyNumberKeyandDash(event, this.value)"  placeholder="03-1111-1111" name="tel" id="tel" class="form-control" value="{{$user->tel}}">
                            <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>
					        <div class="invalid-feedback" id="err_tel" data-custom-validator="数字とハイフンで入力してください"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon12">生年月日</span>
						  </div>
						   <input type="date" class="form-control" name="birthday" id="birthday" aria-describedby="basic-addon12" value="{{ !empty($user->birthday) ? $user->birthday : '1970-01-01'}}">
						  <div class="input-group-append input-label">
						    <span class="input-group-text bg-white border-0" id="basic-addon1">
						    	<i class="fa fa-asterisk fa-1x text-danger"></i>
						    </span>
						  </div>

				    	  <div class="invalid-feedback" id="err_birthday"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end text-center mb-2">
					<div class="col-md-7 col-7">
						<a href="{{ route('account.show', ['id' => $user->id]) }}" class="btn btn-outline-primary">受講者情報へ戻る</a>
					</div>
					<div class="col-md-5 col-5 ">
						<button type="submit" class="btn btn-outline-primary">変更を確定する</button>
					</div>
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	$("#edit_user_account").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		         // console.log(response);
		        if(response.status == true){
			       $("#submit_button").prop('disabled', false);
		           showValidator(response.error,'edit_user_account');
		           window.location = response.redirect;
		        }else{
			        $("#submit_button").prop('disabled', false);
		            showValidator(response.error,'edit_user_account');
		        }
		    },
		    error:function(error){
		        console.log(error)
		    }
		});
	});

    function onlyNumberKeyandDash(evt, tel) {
		  var ASCIICode = (evt.which) ? evt.which : evt.keyCode
		  let last_char = tel.charAt(tel.length - 1);
		if((tel === '' && ASCIICode === 45) || (tel !== '' && ASCIICode === 45 && last_char === '-')){
			return false;
		}else{
			if(ASCIICode === 45){
				return true;
			}
			if ((ASCIICode < 48 || ASCIICode > 57))
				return false;
			return true;
		}
	  }

</script>
@endsection
