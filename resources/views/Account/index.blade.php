@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
	
                <div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">受講者情報</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">氏　名</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="l_name" id="l_name" value="{{ $user->fullname }}" readonly>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">会員番号</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="certificate_id" id="certificate_id" value="{{ $user->certificate_id1 }} - {{ $user->certificate_id2 }}" readonly>
						</div>
					</div>
					
                    <div class="col-md-12 text-center">
                        <a href="{{ $show_user_url}}" class="btn btn-outline-primary">受講者情報の確認</a>
                    </div>
                    
				</div>

                <hr class="page-hr-border">

                <div class="row">

                	<!-- videos -->
                	<div class="col-sm-12 text-center">
						<a href="{{ route('SeminarVideo.index') }}"><b>動画による受講はこちら</b></a>
					</div>

					<!-- form label start -->
					<div class="col-sm-12 text-center">
						<label class="page-row-title">現在お申し込み受付中の講座</label>
					</div>


					@if(!$seminar_groups->isEmpty() && empty($checker))
						@foreach($seminar_groups as $sg)

						 	<div class="col-md-12 text-center mb-2">
		                        <a  href="{{ route('seminar.index', ['seminar_group_id' => $sg->id ])}}" class="btn btn-outline-primary btn-block">{{ $sg->group_name }}</a>
		                    </div>

		                    @endforeach
		            @endif

					@if(!empty($checker))
						@foreach($checker as $sg)
							@if($sg['is_registered'] == false)
								<div class="col-md-12 text-center mb-2">
			                        <a  href="{{ route('seminar.index', ['seminar_group_id' => $sg['id'] ])}}" class="btn btn-outline-primary btn-block">{{ $sg['group_name'] }}</a>
			                    </div>
							@endif
						@endforeach

					@endif

					<div class="col-sm-12 text-center">
						<label class="page-row-title">お申込中の講座</label>
					</div>

					@if(!empty($checker))
						@foreach($checker as $sg)
							@if($sg['is_registered'] == true)
								<div class="col-md-12 text-center mb-2">
			                        <a  href="{{ route('seminar.index', ['seminar_group_id' => $sg['id'] ])}}" class="btn btn-outline-primary btn-block">{{ $sg['group_name'] }}</a>
			                    </div>
							@endif
						@endforeach
					
					@endif

					<div class="col-sm-12 text-center">
						<label class="page-row-title">受講証取得可能な講座</label>
					</div>

					@if(!$course_taken->isEmpty())

						@foreach($course_taken as $row)
							<div class="col-md-12 text-center mb-2">
		                        <a  href="{{ route('SeminarVideo.course.result', ['seminar_id' => $row->m_seminar_id, 'seminar_type' => $row->seminar_type] ) }}" class="btn btn-outline-primary btn-block">{{ $row->seminarGroup->group_name }}</a>
		                    </div>
						@endforeach
					
					@endif

				</div>
		</div>
	</div>
@endsection
