@extends('Layout.qrcode.app')
@section('title', '今後の講座 予定')
@section('css')

<style>
	

</style>
@endsection

@section('content')
	<div class="row justify-content-center" style="margin-top: 165px;">
		<div class="col-md-5 col-12">
            <div class="card">
				<div class="card-body text-primary">
					<div class="row pr-4 pl-4">
						<div class="col-md-4 col-4">
							<i class="fa fa-user fa-3x"></i>
						</div>
						<div class="col-md-8 col-8 mt-2">
							<h4 class="font-weight-bold">{{ $user->l_name }} {{ $user->f_name }}</h4>
						</div>
						
					</div>

					
					<div class="row pr-4 pl-4 mt-2">
						<div class="col-md-12 col-12 mt-2">
							<p>セミナー番号{{ $seminar->category_no }} </p>
							<p>	総論 {{ $seminar->category_no }} {{ $seminar->category_name }}</p>
						</div>

						<div class="col-md-12 text-center">
							
							<i class="fas fa-check" style="font-size: 100px;"></i>
							<p> 回答を受け付けました.</p>

						</div>	

						<div class="col-md-12 col-12 mt-2 text-center">
							<p>退席時も忘れずに　会場の出口にて<span class="text-danger">QRコード退出登録</span>をしてください.</p>
							<a href="{{route('seminar.user.detail',$seminar_registration->id)}}" class="btn btn-outline-primary">講座詳細へ戻る</a>
						</div>

					</div>



				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')


<script type="text/javascript">


</script>

@endsection