@extends('Layout.qrcode.app')
@section('title', '今後の講座 予定')
@section('css')

<style>
	
.div-options {
	 list-style-type:none;
	 margin-bottom: auto;
}

.div-options li {
    /*float:left;*/
    margin:0 5px 0 0;
    min-width:100%;
    /*height:40px;*/
    position:relative;
}

.div-options label.question-choice, .div-options input {
    display:block;
    position:inherit;
   /* top:0;
    left:0;
    right:0;
    bottom:0;*/
}

.div-options input[type="radio"] {
    opacity:0.011;
    z-index:100;
}

.div-options input[type="radio"]:checked + label.question-choice {
    background:#0004FD;
    color: white !important;
} 

.div-options label.question-choice {
 	padding:5px;
 	border:1px solid #0004FD; 
 	cursor:pointer;
    z-index:90;
    border-radius: 5px;
    text-align: center;
    padding: 12px;
    font-weight: normal;
}

.div-options label.question-choice:hover {
     background:#0004FD;
     color: white;
}

.col-md-12.col-12.div-question.is-invalid {
    border: 1px solid #dc3545;
}

.invalid-feedback {
    margin-bottom: 0.25rem;
}

li.options-disabled {
    pointer-events: none;
}


</style>
@endsection

@section('content')
	<div class="row justify-content-center" style="margin-top: 165px;">
		<div class="col-md-5 col-12">
            <div class="card">
				<div class="card-body text-primary">
					<div class="row pr-4 pl-4">
						<div class="col-md-4 col-4">
							<i class="fa fa-user fa-3x"></i>
						</div>
						<div class="col-md-8 col-8 mt-2">
							<h4 class="font-weight-bold">{{ $user->l_name }} {{ $user->f_name }}</h4>
						</div>
						<div class="col-md-12 col-12 mt-2">
							<p>セミナー番号{{ $seminar->category_no }} 総論 {{ $seminar->category_no }} ({{ $seminar->category_name }})</p>
						</div>
					</div>

					<div class="row mb-2">
						<div class="col-md-12 col-12 mt-2 text-center">
							<h5 class="error-div text-danger" style="display: none;"></h5>	
							<h5 class="answering-div">会場画面に表示されている問題に対する回答を答えてください.</h5>
							<h5 class="review-div text-danger" style="display:none;">本当にこの回答でよろしいですね？.
								<span class="font-weight-bold">一度登録した回答は変更できません</span>
							</h5>
						</div>
					</div>

					<form id="exam_form">
						@csrf
						<input type="hidden" name="seminar_registration_id" value="{{ $seminar_registration->id }}">
						<div class="row">

								@if(!$seminar->exams->isEmpty())	
									@foreach($seminar->exams as $key => $eq)
										<div class="col-md-12 col-12 div-question div-question-{{$eq->id}}">
											
											<label class="mb-0">問題 {{ $key+=1 }}</label>
											<ul class="div-options">
												<div class="row">
													
													<div class="col">
														<li>
														    <input type="radio" class="form-control" id="1-{{$eq->id}}" value="1" name="q-{{ $eq->id }}" />
														    <label class="question-choice" for="1-{{$eq->id}}">はい</label>
														</li>
													</div>	
													<div class="col">
														<li>
													    	<input type="radio" class="form-control" id="2-{{$eq->id}}" value="0" name="q-{{ $eq->id }}" />
														    <label class="question-choice" for="2-{{$eq->id}}">いいえ</label>
														</li>
													</div>	
												</div>
												<div class="invalid-feedback" id="err_{{ $eq->id }}"></div>
											</ul>


										</div>
									@endforeach
								@endif

								<div class="col-md-12 text-center mt-2 p-2">
									<button type="button" data-type="review" class="btn btn-outline-primary btn-save review-btn">確認する</button>
									<button type="button" data-type="register" class="btn btn-outline-primary btn-save register-btn" style="display:none;">回答を登録する</button>
								</div>	
								
						</div>

					</form>


				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')


<script type="text/javascript">

	var review = false;

	$(".btn-save").on('click', function(e){
		 e.preventDefault(e);
		var type = $(this).data('type');

		$('.error-div').hide();

		if(type == 'review'){
			
			if(review==false){ //show are you sure etc.
				$('.review-div').show();
				$('.register-btn').show();
				$('.review-btn').text('変更する');
				$('.answering-div').hide();
				//disable fields here
				$("#exam_form input[type=radio]").attr('disabled', true);
				$("#exam_form input[type=radio]").parent().addClass("options-disabled");
				review = true;
			}else{ //hide
				$('.review-btn').text('確認する');
				$('.review-div').hide();
				$('.register-btn').hide();
				$('.answering-div').show();
				
				//enable fields here
				$("#exam_form input[type=radio]").attr('disabled', false);
				$("#exam_form input[type=radio]").parent().removeClass("options-disabled");
				review = false;
			}

			return false;
		}

		$("#exam_form input[type=radio]").attr('disabled', false); //re enable radio before submit
		const data = $('#exam_form').serialize();
	    $.ajax({
		    	type:"POST",
		        url: "{{ route('seminar_exam.store') }}",
		        data:data,
		        cache:false,
		        beforeSend:function(){
		           	$(".register-btn").prop('disabled', true);
		        },
		    	success:function(response){
		        
		        $(".register-btn").prop('disabled', false);
		     	
		     	review = false;

		        if(response.status == true){
		           	window.location = response.redirect;
		        }else{

		        	if(response.type=='answer_all'){
		        		$('.review-div').hide();
						$('.register-btn').hide();
					
		        		$('.error-div').text('全ての問題を解答してください');
	        	 		$('.error-div').show();
		        	}else if(response.type=='answered'){
		        		window.location = response.redirect;
		        	}
		        }		        
		    },
		    error:function(error){
		        console.log(error);
		        $(".register-btn").prop('disabled', false);
		    }
		});
	});


</script>

@endsection