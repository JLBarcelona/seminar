@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<style type="text/css">

</style>
@endsection


@section('content')
	
	<div class="row justify-content-center mt-2">
		<div class="col-sm-5 text-end">
		   <a href="{{ route('account.index') }}" class="btn btn-outline-warning float-right">戻る</a>
		</div>
	</div>
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">

            <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">動画受講講座一覧</label>
				</div>
			</div>

            <div class="row">
				<!-- form label start -->
				<div class="col-sm-12 text-center">
					<label class="page-row-title">動画による受講可能な講座</label>
				</div>

				@if(!$seminarVideos->isEmpty() && empty($checker))
					@foreach($seminarVideos as $row)
					 	<div class="col-md-12 text-center mb-2">
	                        <a  href="{{ route('SeminarVideo.start', ['seminar_id' => $row->seminar_id] ) }}" class="btn btn-outline-primary btn-block">{{ $row->group_name }}</a>
	                    </div>
	                @endforeach
	            @endif

	            @if(!empty($checker))
					@foreach($checker as $row)
						@if($row['is_registered'] == false)
							<div class="col-md-12 text-center mb-2">
		                        <a  href="{{ route('SeminarVideo.start', ['seminar_id' => $row['seminar_id']] ) }}" class="btn btn-outline-primary btn-block">{{ $row['group_name'] }}</a>
		                    </div>
						@endif
					@endforeach
				@endif	

	            <!-- Course Taken -->
	            <div class="col-sm-12 text-center">
					<label class="page-row-title">受講済の講座</label>
				</div>

				@if(!empty($checker))
					@foreach($checker as $row)
						@if($row['is_registered'] == true)
							{{--
								@if(check_seminar_certificate($row['seminar_id']))
									<div class="col-md-12 text-center mb-2">
				                        <a  href="{{ route('SeminarVideo.course.result', ['seminar_id' => $row['seminar_id'], 'seminar_type' => 'video'] ) }}" class="btn btn-outline-primary btn-block">{{ $row['group_name'] }}</a>
				                    </div>
								@else
								@endif
								--}}

							<div class="col-md-12 text-center mb-2">
		                        <a  href="{{ route('SeminarVideo.start', ['seminar_id' => $row['seminar_id']] ) }}" class="btn btn-outline-primary btn-block">{{ $row['group_name'] }}</a>
		                    </div>
						@endif
					@endforeach
				
				@endif

			</div>
		</div>
	</div>
@endsection
