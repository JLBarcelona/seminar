@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<style type="text/css">

</style>
@endsection


@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
            <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">{{ $seminar_detail->group_name }}</label>
				</div>
                
			</div>

            <div class="row">
				<!-- form label start -->
				<div class="col-sm-8">
					<p class="mt-1">動画による受講を開始しますか</p>
				</div>

				<div class="col-sm-4">
					<label class="text-danger">講義時間</label> &emsp;&nbsp; <label>{{ $seminar_detail->video_times }} 分</label>
					<a href="{{ route('SeminarVideo.save.video', ['seminar_id' => $seminar_id] ) }}" class="btn btn-primary btn-block">受講する</a>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-sm-12">
					<p>動画閲覧後、問題が出題されます。回答をお忘れないようにお願いします。</p>
					<p class="text-danger">途中でブラウザの再読み込み、ブラウザを閉じる等を行うと、最初から受講し直しとなります</p>
				</div>
			</div>
			<div class="row justify-content-center mt-2">
				<div class="col-sm-12 text-end">
					<a href="{{ route('account.index') }}" class="btn btn-outline-warning float-right">戻る</a>
				</div>
			</div>

		</div>
	</div>
@endsection
