@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/video.css') }}">
<style>

button.vjs-play-control.vjs-control.vjs-button.vjs-paused {
    visibility: hidden;
}
.vjs-volume-panel.vjs-control.vjs-volume-panel-horizontal {
     visibility: hidden;
}
.vjs-progress-control.vjs-control {
     visibility: hidden;
}
.vjs-remaining-time.vjs-time-control.vjs-control {
     visibility: hidden;
}

button.vjs-picture-in-picture-control.vjs-control.vjs-button {
     visibility: hidden;
}

button.vjs-play-control.vjs-control.vjs-button.vjs-playing {
    visibility: hidden;
}
button.vjs-big-play-button {
     /* display: none; */
     /* visibility: hidden; */
}
/* button.vjs-fullscreen-control.vjs-control.vjs-button {
    flex: auto;
} */

.video-js.vjs-playing .vjs-tech {
  pointer-events: none;
}

</style>
@endsection


@section('content')
	<div class="row">
    <!-- video frame-->
    <div class="col-md-12 col-12">

      @if(!empty($seminar_detail->video->m3u8file_path))
       <div class="col-md-12" id="col-video-container">
          <center><video-js id="video-view" playsinline></video-js>   </center>            
       </div>
      @endif

    </div>
  </div>
			
@endsection

@section('script')
<script src="{{ asset('js/video.js?1') }}"></script>
<script src="{{ asset('js/hls.js?1') }}"></script>

@if(!empty($seminar_detail->video->m3u8file_path))
<script type="text/javascript">

    
$('#col-video-container').click();

  var g_video;
  function initialiseVideo() {

      // disable right click
      $('#video-view').on('contextmenu', function(e) {
          return false;
      });

      // set video
      g_video = new HlsClient(
          'video-view',
          680,     // width
          440,    // height
          [],
          eventHVideoEndFunc
      );

      // set video source
      const src = "{{ $seminar_detail->video->m3u8file_path }}";
      g_video.setSrc(src);
  }

  setTimeout(function(){
    initialiseVideo();
    
    setTimeout(function(){
        playVideo();
    },
      1500);
  },1200);

  function eventHVideoEndFunc() {
      var url = "{{ $redirect }}";
      window.location = url;
  }

  function playVideo() {
      // play video
      g_video.play();
  }


</script>
@endif

@endsection