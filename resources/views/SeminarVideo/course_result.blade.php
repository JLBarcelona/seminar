@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
  <div class="row justify-content-center mt-2">
    <div class="col-sm-5 text-end">
       <a href="{{ route('account.index') }}" class="btn btn-outline-warning float-right">戻る</a>
    </div>
  </div>
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
            <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">受講結果</label>
				</div>
			</div>

            <div class="row">
				<div class="col-sm-12 col-12">
					<div class="input-group mb-3">
					  <div class="input-group-prepend bg-primary input-label">
					    <span class="input-group-text bg-primary border-0" id="basic-addon1">氏名</span>
					  </div>
					  <input type="text" class="form-control bg-white" name="full_name" id="full_name" aria-describedby="basic-addon1" value="{{ $user->fullname }}" readonly>
                      <div class="invalid-feedback" id="err_full_name"></div>
                    </div>
				</div>

				<div class="col-sm-12 col-12">
					<div class="input-group mb-3">
					  <div class="input-group-prepend bg-primary input-label">
					    <span class="input-group-text bg-primary border-0" id="basic-addon2">会員番号</span>
					  </div>
					  <input type="text" class="form-control bg-white" name="membership_number" id="membership_number" aria-describedby="basic-addon2" value="{{ $user->certificate_id1 }} - {{ $user->certificate_id2 }}" readonly>
                      <div class="invalid-feedback" id="err_membership_number"></div>
                    </div>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-sm-12">
					<p><b>{{ $data['group_name'] }} &emsp;&emsp; {{ $data['exam_status'] }}</b></p>
				</div>
			</div>
			
			@if($data['with_certificate'] == true)
				<div class="row justify-content-end">
					<div class="col-md-12 text-right">
						<a type="button" class="btn btn-primary" href="{{url('/certificate_pdf/'.$data['certificate_file'])}}" download="{{ $data['output_file'] }}">証明書を取得する</a>
					</div>
				</div>
			@endif
	</div>
	</div>
@endsection
