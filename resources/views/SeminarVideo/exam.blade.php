@extends('Layout.account.app')
@section('title', '受講者情報')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/video.css') }}">
<style>
.div-options {
	 list-style-type:none;
	 margin-bottom: auto;
}

.div-options li {
    /*float:left;*/
    margin:0 5px 0 0;
    min-width:100%;
    /*height:40px;*/
    position:relative;
}

.div-options label.question-choice, .div-options input {
    display:block;
    position:inherit;
   /* top:0;
    left:0;
    right:0;
    bottom:0;*/
}

.div-options input[type="radio"] {
    opacity:0.011;
    z-index:100;
}

.div-options input[type="radio"]:checked + label.question-choice {
    background:#0004FD;
    color: white !important;
} 

.div-options label.question-choice {
 	padding:5px;
 	border:1px solid #0004FD; 
 	cursor:pointer;
    z-index:90;
    border-radius: 5px;
    text-align: center;
    /* padding: 12px; */
    font-weight: normal;
}

.div-options label.question-choice:hover {
     background:#0004FD;
     color: white;
}

.col-md-12.col-12.div-question.is-invalid {
    border: 1px solid #dc3545;
}

.invalid-feedback {
    margin-bottom: 0.25rem;
}

textarea {
  resize: none;
}

li.options-disabled {
    pointer-events: none;
}

</style>
@endsection


@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
			@if($no_seminar_exam==false)

			<div id="exam_form_row" class="row">
				<!-- exam form-->
				<div class="col-md-12 col-12">

					<div class="card seminar-card border-primary">

					  <div class="card-body text-primary text-center">

						<div class="row mb-2">
							<div class="col-md-12 col-12 mt-2 text-center">
								<h5 class="error-div text-danger" style="display: none;"></h5>	
								{{-- <h5 class="answering-div">会場画面に表示されている問題に対する回答を答えてください.</h5> --}}
								<h5 class="review-div text-danger" style="display:none;">本当にこの回答でよろしいですね？.
									<span class="font-weight-bold">一度登録した回答は変更できません</span>
								</h5>
							</div>
						</div>
			
						<form id="exam_form">
							@csrf
							<input type="hidden" name="user_ondemand_history_id" value="{{ $user_ondemand_history->id }}">
							<div class="row">
			
									@if(!$seminar_detail->exams->isEmpty())	
										@foreach($seminar_detail->exams as $key => $eq)
											<input type="hidden" name="exam_id[]" value="{{$eq->id}}">
											<div class="col-md-12 col-12 div-question div-question-{{$eq->id}}">
												<div class="col-3">
													<p class="mb-0 float-left">問題 {{ $key+=1 }}</p>
												</div>
												<div class="col-12">
													<textarea class="form-control bg-white" rows="3" cols="3" readonly>{{$eq->exam_question}}</textarea>
												</div>
												<div class="col-md-5 col-sm-6 col-lg-5 float-right">
													<ul class="div-options">
														<div class="row" style="max-height:100px;">
															<div class="col">
																<li>
																	<input type="radio" class="form-control" id="1-{{$eq->id}}" value="1" name="q-{{ $eq->id }}" />
																	<label class="question-choice" for="1-{{$eq->id}}">はい</label>
																</li>
															</div>	
															<div class="col">
																<li>
																	<input type="radio" class="form-control" id="2-{{$eq->id}}" value="0" name="q-{{ $eq->id }}" />
																	<label class="question-choice" for="2-{{$eq->id}}">いいえ</label>
																</li>
															</div>	
														</div>
														<div class="invalid-feedback" id="err_{{ $eq->id }}"></div>
													</ul>
												</div>
											</div>
										@endforeach
									@endif
			
									<div class="col-md-12 text-center mt-2 p-2">
										<button type="button" data-type="review" class="btn btn-outline-primary btn-save review-btn">確認する</button>
										<button type="button" data-type="register" class="btn btn-outline-primary btn-save register-btn" style="display:none;">回答を登録する</button>
									</div>	
									
							</div>
			
						</form>
			
					   	
					  </div>

					</div>

				</div>
			</div>

			@else

			
			  
			<div class="col-sm-12 text-center">
				<h5 class="text-danger text-center mb-5">ゼミ試験が見つかりません。</h5>
				<a href="{{route('account.index')}}" class="btn btn-primary">HOMEへ戻る</a>
			</div>

			@endif
			
		</div>
	</div>
@endsection


@section('script')

<script type="text/javascript">
	var review = false;
	$(".btn-save").on('click', function(e){
		 e.preventDefault(e);
		var type = $(this).data('type');
	    
		$('.error-div').hide();

		if(type == 'review'){
			
			if(review==false){ //show are you sure etc.
				$('.review-div').show();
				$('.register-btn').show();
				$('.review-btn').text('変更する');
				$('.answering-div').hide();
				//disable fields here
				$("#exam_form input[type=radio]").attr('disabled', true);
				$("#exam_form input[type=radio]").parent().addClass("options-disabled");
				review = true;
			}else{ //hide
				$('.review-btn').text('確認する');
				$('.review-div').hide();
				$('.register-btn').hide();
				$('.answering-div').show();
				//enable fields here
				$("#exam_form input[type=radio]").attr('disabled', false);
				$("#exam_form input[type=radio]").parent().removeClass("options-disabled");
				review = false;
			}

			return false;
		}

		$("#exam_form input[type=radio]").attr('disabled', false); //re enable radio before submit
		const data = $('#exam_form').serialize();
	    $.ajax({
		    	type:"POST",
		        url: "{{ route('seminar_exam.user_on_demand_exam.store') }}",
		        data:data,
		        cache:false,
		        beforeSend:function(){
		           	$(".register-btn").prop('disabled', true);
		        },
		    	success:function(response){
		        
		        $(".register-btn").prop('disabled', false);
		     	
		     	review = false;

		        if(response.status == true){
		           	window.location = response.redirect;
		        }else{

		        	if(response.type=='answer_all'){
		        		$('.review-div').hide();
						$('.register-btn').hide();
					
		        		$('.error-div').text('全ての問題を解答してください');
	        	 		$('.error-div').show();
		        	}else if(response.type=='answered'){
		        		//window.location = response.redirect;
		        	}
		        }		        
		    },
		    error:function(error){
		        console.log(error);
		        $(".register-btn").prop('disabled', false);
		    }
		});
	});

	//use this to trigger exam show
	// $('#exam_form_row').show();
	
</script>

@endsection