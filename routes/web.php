<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\QrCodeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SeminarController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\SeminarVideoController;
use App\Http\Controllers\SeminarCertificateController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('Auth.index');
})->name('login');

Route::get('/', function () {
    return view('Auth.register');
})->name('registration');

Route::controller(UserController::class)->group(function () {
	Route::get('registration-verify/{token}','showVerifyEmail')->name('registration_verify.get');
	Route::post('registration','store')->name('registration.store');
    Route::get('registration-success', 'success')->name('registration.success');
});

//Authentications/Login
Route::group(['prefix'=> 'auth'], function(){
	Route::name('auth.')->group(function () {
		// For Login
		Route::controller(AuthController::class)->group(function () {
	 		 Route::post('request', 'authenticate')->name('authenticate');
	 		 Route::get('test', 'test')->name('test');
		});

		// For Forgot Password
		Route::controller(ForgotPasswordController::class)->group(function () {
			Route::get('forget-password', 'index')->name('forget.password.get');
			Route::post('forget-password','store')->name('forget.password.post');
			Route::get('reset-password/{token}', 'showResetPasswordForm')->name('reset.password.get');
			Route::post('reset-password', 'submitResetPasswordForm')->name('reset.password.post');
			Route::get('reset-password-success/', 'resetSuccess')->name('reset.password.success');
			Route::get('reset-password-sent/', 'sentResetPasswordPage')->name('reset.password.sent');
		});
	});
});


// Route::get('account/', 'AccountController@index')->name('account.index');

Route::group(['prefix'=> 'account', 'middleware' => ['auth']], function(){
    Route::name('account.')->group(function () {
        // Main Manager Page
        Route::controller(AccountController::class)->group(function () {
            Route::get('','index')->name('index');
            Route::get('show/{id?}','showUserAccount')->name('show');
            Route::get('edit/{id?}','editUserAccount')->name('edit');
            Route::post('update/{id?}','update')->name('update');

            Route::post('account/certificate/','uploadCertificate')->name('upload.certificate');
        });
    });


    Route::name('qrcode.')->group(function () {
        // QRcode
        Route::controller(QrCodeController::class)->group(function () {
            Route::get('qrcode-details/{id}/{seminar_id}','index')->name('index');
            Route::get('check-reception/{id}','checkSeminar')->name('check.seminar');
            Route::get('error-reception/{id}','errorSeminar')->name('error.seminar');
            Route::get('in-reception/{id}','inSeminar')->name('in.seminar');
            Route::get('out-reception/{id}','outSeminar')->name('out.seminar');
        });
    });

    Route::name('seminar.')->group(function () {
        // Seminar Pages
        Route::controller(SeminarController::class)->group(function () {
            Route::get('seminar-list/{seminar_group_id}','index')->name('index'); //this would be seminar list
            Route::get('seminar-detail/{id}','show')->name('show');  //this would be seminar detail
            Route::post('seminar-register','register')->name('register');  //this would be seminar registration
            Route::post('seminar-cancel','cancel')->name('cancel');  //this would be cancel of seminar registration
            Route::get('seminar-register-action', 'register_action')->name('register.action');

            Route::get('seminar-user-detail/{id}','user_detail')->name('user.detail');  //this would be user seminar detail

        });
    });

    Route::name('seminar_exam.')->group(function () {
        // Seminar Exam
        Route::controller(ExamController::class)->group(function () {
            Route::get('seminar-exam/{seminar_registration_id}','index')->name('index');
            Route::post('seminar-exam-store/','store')->name('store');
            Route::get('seminar-exam-success/{seminar_registration_id}','success')->name('success');
            Route::post('seminar-on-demand-exam-store/','userOnDemandExamstore')->name('user_on_demand_exam.store');
        });
    });


    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

});

// video on demand
Route::group(['prefix'=> 'seminar-video', 'as' => 'SeminarVideo.', 'middleware' => ['auth']], function(){
    Route::controller(SeminarVideoController::class)->group(function () {
        Route::get('','index')->name('index');
        Route::get('start-video/{seminar_id}','start_video')->name('start');
        Route::get('save-video/{seminar_id}','save_video')->name('save.video');
        Route::get('play-video/{user_ondemand_history_id}','play_video')->name('play.video');
        Route::get('exam/{user_ondemand_history_id}/{video_finished}','exam')->name('exam');

        Route::get('course-result/{seminar_id}/{seminar_type}','course_result')->name('course.result');
    });
});

// Certificate
// Route::group(['prefix'=> 'certificate', 'as' => 'certificate.', 'middleware' => ['auth']], function(){
//     Route::controller(SeminarCertificateController::class)->group(function () {
//         Route::get('pdf/Certificate', 'pdfCertificate')->name('certificate.pdf');
//     });
// });